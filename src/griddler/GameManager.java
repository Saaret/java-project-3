package griddler;

import griddler.logic.gameManager.gameRoom.*;
import griddler.logic.descriptor.*;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.logic.gameManager.logicEngine.exceptions.*;
import griddler.web.WebManager;
import org.codehaus.jackson.annotate.JsonProperty;

import static griddler.logic.gameLoader.gameMonitorCreator.CreateGameDescriptor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


/**
 * Created by Omri on 12/8/2016.
 * Manages our collection of ongoing games
 */
public class GameManager {
    private List<GameRoom> gameRoomList;
    private LogicEngine logicEngine;
    private HashMap<String, griddler.logic.gameManager.gameRoom.Player> playerHashMap;

    @JsonProperty("gameRoomList")
    public List<GameRoom> getGameRoomList() {
        return gameRoomList;
    }

    public GameManager() {
        logicEngine = new LogicEngine();
        gameRoomList = new LinkedList<GameRoom>();
        playerHashMap = new HashMap<>();
    }

    public LogicEngine getLogicEngine() {
        return logicEngine;
    }

    public void loadNewGame(String gameFilePath) throws GriddlerLogicException, java.io.FileNotFoundException, JAXBException {
        GameDescriptor gameDescriptor;

        /*
        Attempt to load a XML game file, and forwards throw exceptions to calling funct
        for handling
         1. Create the GameDescriptor (requires valid XML file)
         2. Validate that the game is valid (According to the game exercise instructions
         */
        gameDescriptor = CreateGameDescriptor(gameFilePath);
       /*
       We check if the game is valid (exceptions are thrown if not to the user)
       if isValid we create a new game room
        */
        if (logicEngine.validateGameDescriptor(gameDescriptor)) {
            gameRoomList.add(new GameRoom(gameDescriptor));
        }

    }

    /*
    When initializing a game we take the default game (Since we currently only have 1 concurrent game)
    Set it to be a game in progress and Mark the players starting time
     */
    public void InitializeGame() {
        GameRoom defaultGameRoom = gameRoomList.get(0);
        defaultGameRoom.initGame();
    }

    public boolean isGameInProgress() {
        return gameRoomList.get(0).isGameInProgress();
    }

    public boolean isPlayerInCollection(String name) {
        return playerHashMap.containsKey(name);
    }

    /**
     * Params sent from UI, we create a new player and add to table
     *
     * @param name
     * @param isHuman
     * @param sessionID
     */
    public void addPlayerToUserNameCollection(String name, boolean isHuman, String sessionID) {
        Player newPlayer = new Player();
        newPlayer.setName(name);
        newPlayer.setId(sessionID);

        if (isHuman) {
            newPlayer.setPlayerType(Player.PlayerType.HUMAN);
        } else {
            newPlayer.setPlayerType(Player.PlayerType.AI);
        }

        playerHashMap.put(name, newPlayer);
    }

    /**
     * We receive a user name to remove from the userNameCollection
     *
     * @param name of the user to remove
     */
    public void removePlayerToUserNameCollection(String name) {
        playerHashMap.remove(name);
    }

    /**
     * @return an array of the players name in the game
     */
    public String[] getPlayersNameList() {
        String[] playerNameList = new String[playerHashMap.size()];
        int index = 0;

        for (Map.Entry<String, Player> player : playerHashMap.entrySet()) {
            playerNameList[index++] = player.getValue().getName();
        }

        return playerNameList;
    }

    /**
     * Adds a new game room to the end of the gameRoomList
     *
     * @param gameRoom
     */
    public void addNewGameRoom(GameRoom gameRoom) {
        gameRoomList.add(gameRoom);
    }

    /**
     * Check if a game with a duplicate game title exists
     *
     * @param gameTitle
     * @return true if not or false if we found a duplicate
     */
    public boolean isGameWithTitle(String gameTitle) {
        return gameRoomList.stream()
                .filter(gameRoom -> gameRoom.getGameTitle().equals(gameTitle))
                .findFirst().isPresent();
    }

    /**
     * Finds the gameRoom by title
     * If found return it, else return null
     *
     * @param gameTitle
     * @return
     */
    public GameRoom getGameByTitle(String gameTitle) {
        Optional<GameRoom> gameRoomToReturn = gameRoomList.stream()
                .filter(gameRoom -> gameRoom.getGameTitle().equals(gameTitle))
                .findFirst();
        if (gameRoomToReturn.isPresent()) {
            return gameRoomToReturn.get();
        } else {
            return null;
        }
    }

    /**
     * Attempt to add player to game room
     * 1. test that the room exists
     * 2. test that the game isn't already in progress
     * 3. test that the player isn't already in the room
     *
     * @param userName
     * @param gameTitle
     * @return null if we added the player or a error message
     */
    public String addPlayerToGameRoom(String userName, String gameTitle) {
        String errorMessage = null;

        Optional<GameRoom> gameRoomToAddPlayer = gameRoomList.stream()
                .filter(gameRoom -> gameRoom.getGameTitle().equals(gameTitle))
                .findFirst();

        if (!gameRoomToAddPlayer.isPresent()) {
            return "Game Room not found!";
        }

        GameRoom gameRoom = gameRoomToAddPlayer.get();

        if (gameRoom.getPlayersList().contains(playerHashMap.get(userName))) {
            return "Player Already in game!";
        }
        if (gameRoom.isGameFrozen()) {
            return "Game has ended and is frozen!";
        }
        /*
        We check if the game is full
        If full: add to spectator list
        If not add player and check if the game can start
         */
        synchronized (this) {
            if (gameRoom.isFull() || gameRoom.isGameInProgress()) {
                gameRoom.addPlayerToSpectatorList(playerHashMap.get(userName));
            } else {
                gameRoom.addPlayerToPlayerList(playerHashMap.get(userName));
                if (gameRoom.isFull()) {
                    gameRoom.initGame();
                }
            }
        }
        return errorMessage;
    }
}













