package griddler.logic.gameManager.logicEngine;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.board.Block;
import griddler.logic.gameManager.gameRoom.board.CellStatus;
import griddler.logic.gameManager.gameRoom.board.Slice;
import griddler.logic.gameManager.gameRoom.board.Slices;

import java.awt.*;

/**
 * A single threaded version of the perfect block finder
 */
public class PerfectBlockFinderSync {
    GameRoom gameRoom;

    public PerfectBlockFinderSync() {
    }

    public PerfectBlockFinderSync(GameRoom gameRoom) {
        this.gameRoom = gameRoom;
    }

    public void performPerfectBlockSearch(GameRoom gameRoomToSearch) throws Exception {
        this.gameRoom = gameRoomToSearch;
        resetPerfectBlocks();
        updatePerfectBlocks(gameRoomToSearch.getRowSlices(), gameRoomToSearch.getBoardCols() - 1, Slices.Orientation.Rows);
        updatePerfectBlocks(gameRoomToSearch.getColSlices(), gameRoomToSearch.getBoardRows() - 1, Slices.Orientation.Columns);
    }

    private void updatePerfectBlocks(Slices slicesToCheck, int maxIndex, Slices.Orientation orientation) {
        int index = 0;
        Point location;
        for (Slice slice : slicesToCheck.getSliceList()) {
            location = orientation == Slices.Orientation.Rows ? new Point(0, index) : new Point(index, 0);
            checkForPerfectBlocks(slice, maxIndex, orientation, location);
            index++;
        }
    }

    /*
    Resets the board's players slices to "non perfect"
     Need's to be done before finding the perfect blocks for the current player
     */
    private void resetPerfectBlocks() {
        gameRoom.getColSlices().getSliceList().forEach(slice -> slice.getBlocks().forEach(block -> block.setPerfect(false)));
        gameRoom.getRowSlices().getSliceList().forEach(slice -> slice.getBlocks().forEach(block -> block.setPerfect(false)));
    }


    private void checkForPerfectBlocks(Slice blocks, int maxIndex, Slices.Orientation orientation, Point location) {
        Point index;
        boolean countBlockInProcess;
        int blockStartFrom;
        int blockSize;

        for (int j = 0; j < blocks.getBlocks().size(); j++) {
            index = createStartingSearchPoint(location, orientation);
            countBlockInProcess = true;
            blockStartFrom = 0;
            blockSize = 0;
            for (int i = 0; i <= maxIndex; i++) {
                CellStatus currentBlockCellStatus = gameRoom.getCurrentPlayer().getBoardValueAtXY(index.x, index.y).getCellStatus();

                if (countBlockInProcess) {
                    if (currentBlockCellStatus == CellStatus.BLACK) {
                        blockSize++;
                        if (i <= maxIndex) {
                            attemptToMarkPerfectBlock(blockStartFrom, blockSize, blocks);
                        }
                    } else if (currentBlockCellStatus == CellStatus.WHITE) {
                        if (blockSize > 0) {
                            attemptToMarkPerfectBlock(blockStartFrom, blockSize, blocks);
                        }
                        blockSize = 0;
                        blockStartFrom = i + 1;
                    } else {
                        countBlockInProcess = false;
                        blockSize = 0;
                    }
                } else if (currentBlockCellStatus == CellStatus.WHITE) {
                    blockStartFrom = i + 1;
                    countBlockInProcess = true;
                }
                if (orientation == Slices.Orientation.Columns) {
                    index.y++;
                } else {
                    index.x++;
                }
            }
        }
    }


    private void attemptToMarkPerfectBlock(int blockStartFrom, int blockSize, Slice slice) {
        Block candidateBlock = null;
        int numberOfCandidates = 0;

        for (Block block : slice.getBlocks()) {
            if (blockSize == block.getBlock() && !block.isPerfect() && block.getMinRange() <= blockStartFrom && block.getMaxRange() >= blockSize + blockStartFrom - 1) {
                candidateBlock = block;
                numberOfCandidates++;
            }
        }

        if (numberOfCandidates == 1) {
            candidateBlock.setPerfect(true);
            candidateBlock.setStartCell(blockStartFrom);
        }
    }


    /**
     * @param location    starting point of the block we are analyzing
     * @param orientation which slice are we handling
     * @return the point on the board that is beeing tested
     */
    private Point createStartingSearchPoint(Point location, Slices.Orientation orientation) {
        int x = orientation == Slices.Orientation.Columns ? location.x : 0;
        int y = orientation == Slices.Orientation.Columns ? 0 : location.y;

        return new Point(x, y);
    }
}
