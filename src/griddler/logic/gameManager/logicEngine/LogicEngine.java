package griddler.logic.gameManager.logicEngine;

import griddler.logic.descriptor.GameDescriptor;
import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.Move;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.gameRoom.Turn;
import griddler.logic.gameManager.gameRoom.board.CellStatus;
import griddler.logic.gameManager.logicEngine.boardValidations.BoardValidator;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerLogicException;
import griddler.ui.javaFX_UI.controllers.PlayerTableViewDataUnit;
import org.jetbrains.annotations.NotNull;

import javax.servlet.http.Part;
import java.awt.*;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Omri on 12/8/2016.
 */
public class LogicEngine {
    /*
    *Checks that the game descriptor will create a valid game!
     */
    public Boolean validateGameDescriptor(GameDescriptor gameDescriptor) throws GriddlerLogicException {
        Boolean isValid = false;
        BoardValidator boardValidator = new BoardValidator();

        if (gameDescriptor != null) {
            try {
                isValid = boardValidator.isValidGameDescriptor(gameDescriptor);
            } catch (GriddlerLogicException e) {
                throw new GriddlerLogicException(e.getMessage());
            }
        }
        return true;
    }

    /*
    Receives
    @param turn - turn to attempt
    @param= rowSize
    @param = colSize
    @returns = true/false for turn regarding board dimensions
     */
    public Boolean validateTurn(Turn turn, int rowSize, int colSize) {
        Boolean isValid = true;
        for (Move move : turn.getMoves()) {
            if (move.getCol() > colSize || move.getCol() < 0 || move.getRow() < 0 || move.getRow() > rowSize) {
                isValid = false;
            }
        }
        return isValid;
    }

    /*
    Chooses a selection of cells and creates a turn for AI players
     */
    static public Turn getAITurn(int boardCols, int boardRows) {
        List<Move> movesList = new LinkedList<>();
        /*
        each move modifies 10% of the board
         */
        int poolSize = (boardCols * boardRows) / 10;
        Random random = new Random();
        CellStatus[] cellStatusArray = new CellStatus[]{CellStatus.WHITE, CellStatus.BLACK, CellStatus.UNDEFINED};
        CellStatus randomStatus;

        for (int i = 0; i < poolSize; i++) {
            randomStatus = cellStatusArray[random.nextInt(cellStatusArray.length)];
            Move newMove = new Move(random.nextInt(boardCols), random.nextInt(boardRows), randomStatus);
            movesList.add(newMove);
        }

        return new Turn(movesList);
    }

    /*
    returns a linked list of aggregated data for our table view
     */
    public static List<PlayerTableViewDataUnit> getPlayerTableViewDataUnitAsList(GameRoom gameRoom) {
        LinkedList<PlayerTableViewDataUnit> playerTableViewDataUnitLinkedList =
                gameRoom.getPlayersList()
                        .stream()
                        .map(player -> new PlayerTableViewDataUnit(player))
                        .collect(Collectors.toCollection(LinkedList::new));
        return playerTableViewDataUnitLinkedList;
    }

    /*
    recives a collection of cells and creates a turn
     */
    public static Turn createTurnFromSelectedCells(HashSet<Point> selectedCells, CellStatus cellStatus) {
        LinkedList<Move> moveLinkedList = new LinkedList<>();

        for (Point point : selectedCells) {
            Move move = new Move(point.x, point.y, cellStatus);

            moveLinkedList.add(move);
        }

        return new Turn(moveLinkedList);
    }

    @NotNull
    public static String getTurnListAsText(List<Turn> turnList) {
        StringBuilder stringBuilder = new StringBuilder();
        int index = 1;

        for (Turn turn : turnList) {
            stringBuilder.append(index++ + ". " + turn.toString() + "\n");
        }

        return stringBuilder.toString();
    }
}
