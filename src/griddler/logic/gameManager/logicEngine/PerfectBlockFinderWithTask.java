package griddler.logic.gameManager.logicEngine;


import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.board.Block;
import griddler.logic.gameManager.gameRoom.board.CellStatus;
import griddler.logic.gameManager.gameRoom.board.Slices;
import griddler.logic.gameManager.gameRoom.board.Slice;
import javafx.concurrent.Task;

import java.awt.*;
import java.util.List;

/**
 * This Class receives a game board and updates the current players board with the relevant "Perfect Blocks"
 */
public class PerfectBlockFinderWithTask extends Task<Void> {

    GameRoom gameRoom;
    private int SLEEP_TIME;

    public PerfectBlockFinderWithTask(GameRoom gameRoom, int sleepTime) {
        this.gameRoom = gameRoom;
        this.SLEEP_TIME = sleepTime;
    }

    /*
    Runs the perfectBlock test!
     */
    @Override
    protected Void call() throws Exception {
        updateProgress(1, 4);

        updateMessage("Starting the Search for perfect blocks");
        resetPerfectBlocks();
        updateProgress(2, 4);
        sleepForAWhile();

        updateMessage("Searching Horizontally");
        updatePerfectBlocks(gameRoom.getRowSlices(), gameRoom.getBoardCols() - 1, Slices.Orientation.Rows);
        updateProgress(3, 4);

        updateMessage("Searching Vertically");
        updatePerfectBlocks(gameRoom.getColSlices(), gameRoom.getBoardRows() - 1, Slices.Orientation.Columns);
        updateProgress(4, 4);
        sleepForAWhile();

        updateMessage("Done!");
        return null;
    }

    private void updatePerfectBlocks(Slices slicesToCheck, int maxIndex, Slices.Orientation orientation) {
        int index = 0;
        Point location;
        for (Slice slice : slicesToCheck.getSliceList()) {
            location = orientation == Slices.Orientation.Rows ? new Point(0, index) : new Point(index, 0);
            checkForPerfectBlocks(slice, maxIndex, orientation, location);
            index++;
        }
    }

    /*
    Resets the board's players slices to "non perfect"
     Need's to be done before finding the perfect blocks for the current player
     */
    private void resetPerfectBlocks() {
        gameRoom.getColSlices().getSliceList().forEach(slice -> slice.getBlocks().forEach(block -> block.setPerfect(false)));
        gameRoom.getRowSlices().getSliceList().forEach(slice -> slice.getBlocks().forEach(block -> block.setPerfect(false)));
    }


    private void checkForPerfectBlocks(Slice blocks, int maxIndex, Slices.Orientation orientation, Point location) {
        Point index;
        boolean countBlockInProcess;
        int blockStartFrom;
        int blockSize;

        for (int j = 0; j < blocks.getBlocks().size(); j++) {
            index = createStartingSearchPoint(location, orientation);
            countBlockInProcess = true;
            blockStartFrom = 0;
            blockSize = 0;
            for (int i = 0; i <= maxIndex; i++) {
                CellStatus currentBlockCellStatus = gameRoom.getCurrentPlayer().getBoardValueAtXY(index.x, index.y).getCellStatus();

                if (countBlockInProcess) {
                    if (currentBlockCellStatus == CellStatus.BLACK) {
                        blockSize++;
                        if (i <= maxIndex) {
                            attemptToMarkPerfectBlock(blockStartFrom, blockSize, blocks);
                        }
                    } else if (currentBlockCellStatus == CellStatus.WHITE) {
                        if (blockSize > 0) {
                            attemptToMarkPerfectBlock(blockStartFrom, blockSize, blocks);
                        }
                        blockSize = 0;
                        blockStartFrom = i + 1;
                    } else {
                        countBlockInProcess = false;
                        blockSize = 0;
                    }
                } else if (currentBlockCellStatus == CellStatus.WHITE) {
                    blockStartFrom = i + 1;
                    countBlockInProcess = true;
                }
                if (orientation == Slices.Orientation.Columns) {
                    index.y++;
                } else {
                    index.x++;
                }
            }
        }
    }


    private void attemptToMarkPerfectBlock(int blockStartFrom, int blockSize, Slice slice) {
        Block candidateBlock = null;
        int numberOfCandidates = 0;

        for (Block block : slice.getBlocks()) {
            if (blockSize == block.getBlock() && !block.isPerfect() && block.getMinRange() <= blockStartFrom && block.getMaxRange() >= blockSize + blockStartFrom - 1) {
                candidateBlock = block;
                numberOfCandidates++;
            }
        }

        if (numberOfCandidates == 1) {
            candidateBlock.setPerfect(true);
            candidateBlock.setStartCell(blockStartFrom);
        }
    }


    private void sleepForAWhile() {
        if (SLEEP_TIME != 0) {
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException ignored) {

            }
        }
    }

    /**
     * @param location    starting point of the block we are analyzing
     * @param orientation which slice are we handling
     * @return the point on the board that is beeing tested
     */
    private Point createStartingSearchPoint(Point location, Slices.Orientation orientation) {
        int x = orientation == Slices.Orientation.Columns ? location.x : 0;
        int y = orientation == Slices.Orientation.Columns ? 0 : location.y;

        return new Point(x, y);
    }
}
