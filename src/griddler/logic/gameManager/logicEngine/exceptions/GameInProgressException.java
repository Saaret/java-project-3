package griddler.logic.gameManager.logicEngine.exceptions;

/**
 * Created by Omri on 12/8/2016.
 */
public class GameInProgressException extends GriddlerLogicException {
    public Exception innerException;

    public GameInProgressException() {
        message = "Game already in progress!";
    }
}
