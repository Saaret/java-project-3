package griddler.logic.gameManager.logicEngine.exceptions;

/**
 * Created by Omri on 13/8/2016.
 */
public class GriddlerValidBoardException extends GriddlerLogicException {
    public GriddlerValidBoardException() {
        message = "Board invalid!";
    }

    public GriddlerValidBoardException(String s) {
        message = s;
    }
}
