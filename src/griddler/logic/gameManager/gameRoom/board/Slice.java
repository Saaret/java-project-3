package griddler.logic.gameManager.gameRoom.board;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Omri on 28/9/2016.
 */
public class Slice {
    List<Block> blocks;
    int id;

    Slice() {
        blocks = new ArrayList<>();
    }

    /*
    So that we don't update the possible starting or ending index of the blocks we do it once
     */
    public void UpdateBlocksInSlice(int maxIndex) {
        int minRange = 0;
        int maxRange = maxIndex - 1;

        for (int i = 0; i < blocks.size(); i++) {
            blocks.get(i).setMinRange(minRange);
            blocks.get(blocks.size() - i - 1).setMaxRange(maxRange);
            minRange += 1 + blocks.get(i).getBlock();
            maxRange -= 1 + blocks.get(blocks.size() - i - 1).getBlock();
        }
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public void add(int block) {
        blocks.add(new Block(block));
    }

    public int getSize() {
        return blocks.size();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
