package griddler.logic.gameManager.gameRoom;

import griddler.logic.descriptor.*;
import griddler.logic.gameManager.gameRoom.board.*;
import griddler.logic.gameManager.gameRoom.board.Slice;
import griddler.logic.gameManager.gameRoom.board.Slices;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.logic.gameManager.logicEngine.PerfectBlockFinderSync;
import griddler.logic.gameManager.logicEngine.PerfectBlockFinderWithTask;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerLogicException;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUI_GameStaticParams;
import javafx.concurrent.Task;

import java.util.*;

/**
 * Created by Omri on 12/8/2016.
 * Manages a single game room
 * Is built to hold: A single game board + dynamic number of players
 */
public class GameRoom {
    private String gameTitle;
    private List<Player> playersList;
    private List<Player> spectatorList;
    private Player currentPlayer;
    private String uploadedBy;
    private Board board;
    private LogicEngine gameLogic = new LogicEngine();
    /*Marks an initialized game*/
    private boolean gameInProgress = false;
    private boolean gameFrozen = false;
    private String winningPlayer;
    private int maxNumberOfPlayers;
    private int maxNumberOfTurns;
    private int currentNumberOfTurns = 0;
    private boolean isSinglePlayerGame;

    public GameRoom() {
    }

    /*
    We assume that the game descriptor is valid (has been checked by our logic module)
    */
    public GameRoom(GameDescriptor gameDescriptor) {
        gameTitle = gameDescriptor.getDynamicMultiPlayers().getGametitle();
        board = new Board(gameDescriptor.getBoard());

        playersList = getPlayersListFromGameDescriptor(gameDescriptor.getMultiPlayers());
        spectatorList = new LinkedList<>();
        maxNumberOfTurns = Integer.parseInt(gameDescriptor.getMultiPlayers().getMoves());
        currentPlayer = playersList.get(0);
    }

    /**
     * Fills a game room with starting values
     * Works for the exercise 3 files!
     *
     * @param gameDescriptor a parsed JAXB file
     */
    public void setFieldsFromGameDescriptor(GameDescriptor gameDescriptor) throws GriddlerLogicException {
        if (gameLogic.validateGameDescriptor(gameDescriptor)) {
            gameTitle = gameDescriptor.getDynamicMultiPlayers().getGametitle();
            board = new Board(gameDescriptor.getBoard());
            maxNumberOfPlayers = Integer.parseInt(gameDescriptor.getDynamicMultiPlayers().getTotalPlayers());
            playersList = new ArrayList<>(maxNumberOfPlayers);
            spectatorList = new LinkedList<>();
            maxNumberOfTurns = Integer.parseInt(gameDescriptor.getDynamicMultiPlayers().getTotalmoves());
        }
    }

    private List<Player> getPlayersListFromGameDescriptor(GameDescriptor.MultiPlayers multiPlayers) {
        int mutliPlayersSize = multiPlayers.getPlayers().getPlayer().size();
        List<Player> playerList = new ArrayList<>(mutliPlayersSize);
        /*
        create the player list with each "node" a human or computer player
         */
        if (mutliPlayersSize != 0) {
            for (int i = 0; i < mutliPlayersSize; i++) {
                if (multiPlayers.getPlayers()
                        .getPlayer()
                        .get(i)
                        .getPlayerType()
                        .equalsIgnoreCase("Human")) {
                    playerList.add(new HumanPlayer(multiPlayers.getPlayers().getPlayer().get(i), board.getNumOfCols(), board.getNumOfRows()));
                } else if (
                        multiPlayers.getPlayers()
                                .getPlayer()
                                .get(i)
                                .getPlayerType()
                                .equalsIgnoreCase("Computer")) {
                    playerList.add(new AIPlayer(multiPlayers.getPlayers().getPlayer().get(i), board.getNumOfCols(), board.getNumOfRows(), Integer.parseInt(multiPlayers.getMoves())));
                }
            }
        } else {
            playerList = null;
        }

        return playerList;
    }

    public void setGameInProgress(boolean gameInProgress) {
        this.gameInProgress = gameInProgress;
    }

    public boolean isGameInProgress() {
        return gameInProgress;
    }

    public void initGame() {

        //Init players!
        getPlayersList().forEach(player -> player.initializePlayerBoard(board.getNumOfCols(), board.getNumOfRows()));
        getPlayersList().forEach(player -> player.init(board.getSolution()));
        //Init Slices with block index
        for (Slice s : board.getColSlices().getSliceList()) {
            s.UpdateBlocksInSlice(board.getNumOfCols());
        }
        for (Slice s : board.getRowSlices().getSliceList()) {
            s.UpdateBlocksInSlice(board.getNumOfRows());
        }
        setGameInProgress(true);
        isSinglePlayerGame = playersList.size() == 1;
        currentPlayer = playersList.get(0);
    }


    public boolean validateTurn(Turn turnToPerform) {
        /*
        We check each move in the turn for validity
        if all are valid -> the move is OK
         */
        return turnToPerform.getMoves().stream().
                allMatch(move -> move.getRow() < getBoardRows() && move.getCol() < getBoardCols());
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /*
    Finds the player position and advances the the next one or sends us back to the beginning of the list
     */
    public void advanceToNextPlayer() {
        Iterator<Player> iterator = playersList.iterator();
        Player player;

        while (iterator.hasNext()) {
            player = iterator.next();
            if (player.equals(currentPlayer)) {
                if (iterator.hasNext()) {
                    currentPlayer = iterator.next();
                } else {
                    currentPlayer = playersList.get(0);
                    currentNumberOfTurns++;
                    //Check if the game ended by turns
                    if (currentNumberOfTurns == maxNumberOfTurns) {
                        gameInProgress = false;
                        winningPlayer = null;
                    }
                }
            }
        }
    }

    /**
     * Receive the players name and return null or the player
     *
     * @param name
     * @return
     */
    public Player getPlayerByName(String name) {
        Player playerToReturn;
        Optional<Player> playerOptional = playersList.stream()
                .filter(player -> player.getName().equals(name))
                .findFirst();

        if (playerOptional.isPresent()) {
            playerToReturn = playerOptional.get();
        } else {
            playerToReturn = null;
        }
        return playerToReturn;
    }

    /**
     * Receive the spectators name and return null or the player
     *
     * @param name
     * @return
     */
    public Player getSpectatorByName(String name) {
        Player spectatorToReturn;
        Optional<Player> spectatorOptional = spectatorList.stream()
                .filter(spectator -> spectator.getName().equals(name))
                .findFirst();

        if (spectatorOptional.isPresent()) {
            spectatorToReturn = spectatorOptional.get();
        } else {
            spectatorToReturn = null;
        }
        return spectatorToReturn;
    }

    /**
     * We receive a userName and remove it from both the spectators list and the players list
     *
     * @param userName
     */
    public void removeUserFromGameRoom(String userName) {
        Player playerToRemove = getPlayerByName(userName);
        Player spectatorToRemove = getSpectatorByName(userName);

        if (playerToRemove != null) {
            playersList.remove(playerToRemove);
        }

        if (spectatorToRemove != null) {
            spectatorList.remove(spectatorToRemove);
        }
    }

    /**
     * Starts new thread for handling the perfect block update
     */
    public void runPerfectBlockFinder() throws Exception {
        PerfectBlockFinderSync blockFinder = new PerfectBlockFinderSync();
        blockFinder.performPerfectBlockSearch(this);
    }

    /**
     * After a game ends we may want to perserve the game room but
     * default it back to the point before it wan initialized
     */
    public void resetGameRoom() {
        playersList = new ArrayList<>(maxNumberOfPlayers);
        spectatorList = new LinkedList<>();
        setGameFrozen(false);
        setGameInProgress(false);
    }

    public boolean isFull() {
        return maxNumberOfPlayers == playersList.size();
    }

    public void addPlayerToSpectatorList(Player player) {
        spectatorList.add(player);
    }

    public void addPlayerToPlayerList(Player player) {
        playersList.add(player);
    }

    public int getBoardCols() {
        return board.getNumOfCols();
    }

    public int getBoardRows() {
        return board.getNumOfRows();
    }

    public List<Player> getPlayersList() {
        return playersList;
    }

    public Slices getRowSlices() {
        return board.getRowSlices();
    }

    public Slices getColSlices() {
        return board.getColSlices();
    }

    public String getGameTitle() {
        return gameTitle;
    }

    public int getMaxNumberOfTurns() {
        return maxNumberOfTurns;
    }

    public int getMaxNumberOfPlayers() {
        return maxNumberOfPlayers;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public int getCurrentNumberOfPlayers() {
        return playersList.size();
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public String getWinningPlayer() {
        return winningPlayer;
    }

    public void setWinningPlayer(String winningPlayer) {
        this.winningPlayer = winningPlayer;
    }

    public boolean isSinglePlayerGame() {
        return isSinglePlayerGame;
    }

    public List<Player> getSpectatorList() {
        return spectatorList;
    }

    public boolean isGameFrozen() {
        return gameFrozen;
    }

    public void setGameFrozen(boolean gameFrozen) {
        this.gameFrozen = gameFrozen;
    }

    public int getCurrentNumberOfTurns() {
        return currentNumberOfTurns;
    }
}

