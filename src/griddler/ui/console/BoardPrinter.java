package griddler.ui.console;

import griddler.logic.gameManager.gameRoom.*;
import griddler.logic.gameManager.gameRoom.board.Block;

/**
 * Created by Omri on 19/8/2016.
 * Prints a board!
 */
class BoardPrinter {

    static void printBoard(GameRoom gameRoom) {

        printColumnNumbers(gameRoom);
        printHorizontalBorder(gameRoom);
        printCellsAndHorizontalSlices(gameRoom, gameRoom.getPlayersList().get(0));
        printHorizontalBorder(gameRoom);
        printVerticalSlices(gameRoom);
    }

    private static void printColumnNumbers(GameRoom gameRoom) {
        System.out.print("   ");
        for (int i = 0; i < gameRoom.getBoardCols(); i++) {
            System.out.print(String.format(" %s ", i));
        }
        System.out.println();

    }

    private static void printVerticalSlices(GameRoom gameRoom) {

        int maxNumOfVerticalBlocks = 0, currentSizeOfSlice, verticalIndex = 0;
        /*
        We get the largest number of blocks in our vertical slices so we can limit printing
         */
        for (int i = 0; i < gameRoom.getBoardCols(); i++) {
            currentSizeOfSlice = gameRoom.getColSlices().getSliceInList(i).getSize();
            if (currentSizeOfSlice > maxNumOfVerticalBlocks) {
                maxNumOfVerticalBlocks = currentSizeOfSlice;
            }
        }
        /*
        Printing the slices!
         */
        for (int i = 0; i < maxNumOfVerticalBlocks; i++) {
            System.out.print("   ");
            for (int j = 0; j < gameRoom.getBoardCols(); j++) {
                String stringToPrint;
                if (gameRoom.getColSlices().getSliceList().size() > i) {
                    stringToPrint = gameRoom.getColSlices().getSliceInList(j).getBlocks().get(i).toString();
                } else {
                    stringToPrint = " ";
                }
                if (j < 9) {
                    System.out.print(String.format(" %s ", stringToPrint));
                } else {
                    System.out.print(String.format(" %s  ", stringToPrint));
                }

            }
            System.out.println();
        }
    }

    /*
    Printing the current game board and only horizontal slices
    */
    private static void printCellsAndHorizontalSlices(GameRoom gameRoom, Player player) {
        /*
        print the i'th row + it's slices!
        */
        for (int i = 0; i < gameRoom.getBoardRows(); i++) {

            System.out.print(String.format("%s |", i));
            for (int j = 0; j < gameRoom.getBoardCols(); j++) {
                if (j < 9) {
                    System.out.print(String.format(" %s ", player.getBoardValueAtXY(j, i).toString()));
                } else {
                    System.out.print(String.format(" %s  ", player.getBoardValueAtXY(j, i).toString()));
                }
            }
            System.out.print(" || ");
            for (Block block : gameRoom.getRowSlices().getSliceInList(i).getBlocks()) {
                System.out.print(String.format(" %s ", block.getBlock()));
            }
            System.out.println();
        }
    }

    private static void printHorizontalBorder(GameRoom gameRoom) {
       /*
       Printing the top border!
        */
        for (int i = 0; i < gameRoom.getBoardCols(); i++) {
            System.out.print("===");
            if (i > 9) {
                System.out.print("==");
            }
        }
        System.out.println();
    }

}
