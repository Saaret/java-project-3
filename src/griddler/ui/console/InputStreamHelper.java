package griddler.ui.console;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Omri on 20/8/2016.
 * For reading files from within the JAR, prep for exercise 2
 */
class InputStreamHelper {
    BufferedReader getBufferedReader(String path) {
        BufferedReader bufferedReader;
        InputStream inputStream = getClass().getResourceAsStream(path);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        bufferedReader = new BufferedReader(inputStreamReader);

        return bufferedReader;
    }
}
