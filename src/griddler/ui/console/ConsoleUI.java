package griddler.ui.console;

import griddler.GameManager;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.gameRoom.Turn;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.logic.gameManager.logicEngine.exceptions.*;

import java.util.List;
import java.util.Scanner;

import static griddler.ui.console.menuSelectionNumbers.*;


/**
 * Created by Omri on 12/8/2016.
 * Handles printing to the console and receiving user inputs
 * Gives input to game manager.
 */
class ConsoleUI {
    /*
    inner field so not to hard code the game menu
     */
    private static final String gameMenuPath = "src/resources/gameConsoleMenu.txt";
    private static boolean gameEnded = false;
    private static GameManager gameManager;
    // Create static scanner to handle all consoleUI input
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String args[]) {
        playGame();
    }

    private static void playGame() {

        while (!gameEnded) {
            printMenu();
            handleUserInput();
            System.out.println("Press enter to continue");
            scanner.nextLine();
        }
    }

    private static void handleUserInput() {
        String input;
        int userSelection;
        input = scanner.nextLine();

        if (isValidNumber(input)) {
            activateMenuItem(Integer.parseInt(input));
        } else {
            System.out.println("Invalid input, please input a number between 1-9");
        }
    }

    private static void activateMenuItem(int userInput) {
        menuSelectionNumbers userInputAsEnum = menuSelectionNumbers.values()[userInput];

        switch (userInputAsEnum) {
            case LOAD_A_NEW_GAME:
                loadNewGame();
                break;
            case BEGIN_GAME:
                initConsoleGame();
                break;
            case SHOW_CURRENT_GAME_STATUS:
                printBoard();
                break;
            case PERFORM_MOVE:
                performMove();
                break;
            case PRINT_MOVE_LIST:
                printPlayerMoveList();
                break;
            case UNDO:
                undoLastPlayerMove();
                break;
            case STATS:
                printGameStats();
                break;
            case END_GAME:
                endConsoleGame();
                break;
            case REDO:
                redoLastTurn();
                break;
            default:
                break;
        }
    }

    private static void redoLastTurn() {
        if (gameIsInitialized()) {
            gameManager.getGameRoomList().get(0).getPlayersList().get(0).performRedo();
            printBoard();
        } else {
            System.out.println("No Game in progress!");
        }
    }

    private static void endConsoleGame() {
        // checkign that a game exists
        if (gameManager != null) {
            printGameStats();
        }
        System.out.println("Game Has Ended!");
        System.out.println("If you would like to load another game press y then press enter,\n if you would like to quit press any other key");
        String userInput = scanner.next();

        if (userInput.equals("y")) {
            gameManager = null;
            System.out.println("Game closed, returning to main menu, press enter to continue");
        } else {
            gameEnded = true;
            System.out.println("Game closing, Goodbye!");
        }
    }

    /*
    Have the player return our stat's information
     */
    private static void printGameStats() {
        if (gameIsInitialized()) {
            Player playerToGetStatsFor = gameManager.getGameRoomList().get(0).getPlayersList().get(0);
            System.out.println(String.format("Player %s Statistics!", playerToGetStatsFor.getName()));
            System.out.println((String.format("The player has performed %s moves", playerToGetStatsFor.getStats().getMovesPerformedCount())));
            System.out.println(String.format("The player has performed %s undoes", playerToGetStatsFor.getStats().getUndoesPerformedCount()));
            System.out.println(playerToGetStatsFor.getStats().getGameStartTime());
            System.out.println(String.format("Player score is %s/100.", playerToGetStatsFor.getStats().getScore()));
        } else {
            System.out.println("Game not initialized!");
        }
    }

    /*
    Undoig the last player's move:
    1. Ask the player to undo the last move!
    */
    private static void undoLastPlayerMove() {
        gameManager.getGameRoomList().get(0).getPlayersList().get(0).performUndo();
        /*
        Now we print the update board for the user
         */
        printBoard();
    }

    private static void printPlayerMoveList() {
        List<Turn> turnList = gameManager.getGameRoomList().get(0).getPlayersList().get(0).getTurnList();
        int index = 0;
        for (Turn currentTurn : turnList) {
            System.out.println(String.format("Turn number %s: %s.", ++index, currentTurn.toString()));
        }
    }

    /*
    receive an move via console from player and
    Will be migrated to the Game room once the possibility of playing one against eachother is needed in exercise 2
    at that point the creation of the turn will bne different but the methods needed for each type of player to play will not require any additional modification
    If the AI is playing - a turn is generated and passed to the player
     */
    private static void performMove() {
        if (gameIsInitialized()) {
            Player player = gameManager.getGameRoomList().get(0).getPlayersList().get(0);
            Turn newTurn = null;
            try {
                if (player.getPlayerType().equals(Player.PlayerType.HUMAN)) {
                    newTurn = getTurnFromConsole();
                    player.performTurn(newTurn);
                } else if (player.getPlayerType().equals(Player.PlayerType.AI)) {
                    simulateAIGame(player, gameManager.getGameRoomList().get(0).getBoardCols(), gameManager.getGameRoomList().get(0).getBoardRows());
                }
                printBoard();
            } catch (GriddlerLogicException e) {
                System.out.println(e.getMessage());
            }
            // checking if game has ended
            if (player.isGameWon()) {
                endConsoleGame();
            } else if (player.getStats().getScore() == 100) {
                System.out.println("You have painted more cells black then are needed, paint some of them white... :(");
            }
        }
    }

    /*
    Since the actual game is played in turns and in the console only the game is played entirely in the backround
    We have a AI play "Moves" amount of turns and then end the game
     */
    private static void simulateAIGame(Player player, int boardCols, int boardRows) {
        Turn simulatedTurn;
        while (player.canPlay()) {
            simulatedTurn = LogicEngine.getAITurn(boardCols, boardRows);
            player.performTurn(simulatedTurn);
        }
        player.setUndefinedToWhite();
    }

    private static Turn getTurnFromConsole() throws GriddlerLogicException {
        String turnToParse;
        Turn turnToPerform;
        do {
            System.out.println("Please enter your move");
            turnToParse = scanner.next();
            turnToPerform = Turn.parse(turnToParse);
            /*
            Validate board relative to gameRoom
             */
        } while (!gameManager.getGameRoomList().get(0).validateTurn(turnToPerform));

        return turnToPerform;
    }

    /*
    For checking before performing actions on a board that may not be initialized
     */
    private static boolean gameIsInitialized() {
        return gameManager != null && gameManager.getGameRoomList().get(0).isGameInProgress();
    }

    private static void initConsoleGame() {
        if (gameManager == null) {
            System.out.println("Game not loaded yet!");
        } else {
            gameManager.InitializeGame();
            System.out.println("Game initialized successfully.");
        }
    }

    /*
    prints current playerBoard
   */
    private static void printBoard() {
        BoardPrinter.printBoard(gameManager.getGameRoomList().get(0));
    }

    /*
    Instantiates gameManager and opens a new game.
    If already created notifies to user.
    Since there are limitations in Ex.1 I choose to do this in the console UI while having
    the game loader from the logic package handle the "actual loading" and throwing exceptions to the UI when needed
     */
    private static void loadNewGame() {

        if (gameManager != null) {
            handleLoadedGame();

        } else {
            gameManager = new GameManager();
            String gameFilePath;

            System.out.println("Please supply a full path to the game file you wish to load");
            gameFilePath = scanner.nextLine();
            /*
            We attempt to load a new game
            An exception is thrown if an XML file is invlid for some reason
             */
            try {
                gameManager.loadNewGame(gameFilePath);
                System.out.println("Game loaded successfully!");
            } catch (NullPointerException e) {
                System.out.println("Invalid board, slice missing blocks definition!");
                gameManager = null;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                gameManager = null;

            }
        }
    }

    /*If a game has been loaded i.e. a gameRoom has been created, we let the user dicide to cancel or load a new game*/

    private static void handleLoadedGame() {
        boolean inputDone = false;
        int userInput = 0;

        if (gameManager.isGameInProgress()) {
            System.out.println("Game already in progress!");
        } else {
            System.out.println("A game has been loaded but has not yet begun, enter 1 to load a new game or 2 to cancel");
        }
        /*
        we collect the "valid" input from the user
         */
        while (!inputDone) {
            try {
                userInput = scanner.nextInt();
            } catch (NumberFormatException e) {
                System.out.println("Not a valid Number! Try again....");
            }

            switch (userInput) {
                case 1:
                    gameManager = null;
                    loadNewGame();
                    inputDone = true;
                    break;
                case 2:
                    System.out.println("Returning to main menu!");
                    inputDone = true;
                    break;
            }
        }
    }

    /*
    *Checks user input,
    * Tests:
    *   1. If it's a number
    *   2. If it's in the range of the menu selection
     */
    private static boolean isValidNumber(String input) {
        boolean isValid = true;
        int userIntInput;
        try {
            userIntInput = Integer.parseInt(input);
            if (userIntInput >= BEGIN_GAME.getSelection() && userIntInput < REDO.getSelection()) {
                isValid = true;
            }
        } catch (Exception e) {
            isValid = false;
        }

        return isValid;
    }

    private static void printMenu() {
        String string = "Griddler Game Instructions!\n" +
                "1. [Load a new Game] - Selecting this option will require you to supply a complete path to a XML game file.\n" +
                "2. [Begin game] - After succssfully loading a game, this will allow you to begin playing\n" +
                "3. [Show Current Game Status] - Presents the current board status(printed to screen\\console).\n" +
                "4. [Perform move] - Allows updating the game board with a move of your choice.\n" +
                "                    For example to change cells 2-4 in row 1 to black:\n" +
                "                        \"r>>1>>2>>4>>b\"\n" +
                "5. [Print move list] - Prints the move list up to this point (doesn't include \"redo\" or \"undo\" moves).\n" +
                "6. [Undo] - Will undo the last move performed and update the game board accordingly.\n" +
                "7. [Stats] - Will print the following player stats to the console: number of completed moves, number of undo moves, elapsed time from game stat and player score.\n" +
                "8. [End game] - Prints the player status and closes the game.\n" +
                "9. [Redo] - Re-does the last undone move.\n";

        System.out.print(string);
    }

/*
If you want to have the detailied menu printed each time uncomments this method
 */
  /*  private static void printMenu() {

        try (BufferedReader br = new BufferedReader(new FileReader(new File(gameMenuPath)))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (Exception e) {
            System.out.println("Menu file not found in resources package, please check folder.");
        }
    }*/
}
