package griddler.ui.javaFX_UI.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

import java.io.IOException;

/**
 * Created by Omri on 20/9/2016.
 */
public class CustomTextMessagePane extends Pane {
    @FXML
    private Label customTextPlacement;

    public CustomTextMessagePane(String s) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CustomTextPane.fxml"));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        customTextPlacement.setText(s);
    }
}
