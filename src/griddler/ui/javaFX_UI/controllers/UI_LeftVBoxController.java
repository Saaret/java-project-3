package griddler.ui.javaFX_UI.controllers;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUIController;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUI_GameStaticParams;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class UI_LeftVBoxController extends VBox implements Initializable, CustomGriddlerController {
    @FXML
    private VBox containerVBox;
    @FXML
    private Button peekAtBoardButton;
    @FXML
    private Label playerNameLabel;
    @FXML
    private TableView<PlayerTableViewDataUnit> playerTable;
    @FXML
    private TableColumn<PlayerTableViewDataUnit, String> nameColumn;
    @FXML
    private TableColumn<PlayerTableViewDataUnit, String> playerScoreColumn;
    @FXML
    private TableColumn<PlayerTableViewDataUnit, String> playerTypeColumn;
    @FXML
    private TableColumn<PlayerTableViewDataUnit, String> playerIDColumn;
    @FXML
    private Button showSelectedPlayersTurnListButton;

    private Button showReplay;
    private ObservableList<PlayerTableViewDataUnit> playerTableViewObservableList;

    public UI_LeftVBoxController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LeftPaneVBox.fxml"));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /*
    Initializing a game start
     */
    public void initLeftVBoxController(GameRoom gameRoom) {
        setFirstPlayerName(gameRoom.getPlayersList());
        initializePlayerTableViewObservableList(gameRoom);
        initializePlayerScoreTable(gameRoom);
        containerVBox.setDisable(false);

    }

    private void setFirstPlayerName(List<Player> playersList) {

        for (Player player : playersList) {
            if (player.getPlayerType() == Player.PlayerType.HUMAN) {
                playerNameLabel.setText(player.getName());
                return;
            }
        }
        //If the players are all AI
        playerNameLabel.setText(playersList.get(0).getName());
    }

    /*
    Creates an observable list for our table view
     */
    private void initializePlayerTableViewObservableList(GameRoom gameRoom) {
        playerTableViewObservableList = FXCollections.observableArrayList();
        List<PlayerTableViewDataUnit> playerTableViewDataUnitList = LogicEngine.getPlayerTableViewDataUnitAsList(gameRoom);
        playerTableViewObservableList.addAll(playerTableViewDataUnitList);
    }

    /*
    assumes that the observable list is initialized
     */
    private void initializePlayerScoreTable(GameRoom gameRoom) {
        if (playerTableViewObservableList != null) {
            playerTable.setItems(playerTableViewObservableList);
        }
    }

    /*
    Get's a player and updates the board!
     */
    public void updateCurrentPlayerStats(Player currentPlayer) {
        String currentPlayerName = currentPlayer.getName();
        playerNameLabel.setText(currentPlayerName);
    }

    @Override
    public void setControlsOnEvent(GriddlerGUIController griddlerGUIController) {
        peekAtBoardButton.setOnAction(griddlerGUIController);
        showSelectedPlayersTurnListButton.setOnAction(griddlerGUIController);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getPlayerNameProperty());
        playerTypeColumn.setCellValueFactory(cellData -> cellData.getValue().getPlayerTypeProperty());
        playerScoreColumn.setCellValueFactory(cellData -> cellData.getValue().getPlayerScoreProperty());
        playerIDColumn.setCellValueFactory(cellData -> cellData.getValue().getPlayerIDProperty());
        showSelectedPlayersTurnListButton.setDisable(true);

        addListenerToPlayerTableView(showSelectedPlayersTurnListButton);

        //Add change listener
        playerTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            //Check whether item is selected and enable/disable the peek button

            if (!GriddlerGUI_GameStaticParams.isGameInProgress() || playerTable.getSelectionModel().getSelectedItem().getPlayerType().equals("AI")) {
                peekAtBoardButton.setDisable(false);
            } else {
                peekAtBoardButton.setDisable(true);
            }
        });
    }

    public void addListenerToPlayerTableView(Button button) {
        //Add change listener
        playerTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            //Check whether item is selected and enable/disable the peek button
            if (playerTable.getSelectionModel().getSelectedCells() == null) {
                button.setDisable(true);
            } else {
                button.setDisable(false);
            }
        });
    }

    /*
    We get the relevant player and update his score for the table view
     */
    public void updateScoreBoard(Player currentPlayer) {

        PlayerTableViewDataUnit playerInTableView =
                playerTableViewObservableList
                        .filtered((e) ->
                                e.getPlayerID().equals(currentPlayer.getIDAsString()))
                        .get(0);
        playerInTableView.setPlayerScore(currentPlayer.getStats().getScore());
    }

    /*
    Gets the focused player's ID
     */
    public String getSelectedPlayerIDInTableView() {
        return playerTable.getSelectionModel().getSelectedItems().get(0).getPlayerID();
    }


    public Button getPeekAtBoardButton() {
        return peekAtBoardButton;
    }

    public Button getShowSelectedPlayerTurnListButton() {
        return showSelectedPlayersTurnListButton;
    }
}

