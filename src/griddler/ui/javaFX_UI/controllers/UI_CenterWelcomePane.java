package griddler.ui.javaFX_UI.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.io.IOException;

public class UI_CenterWelcomePane extends Pane {

    public UI_CenterWelcomePane() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CustomTextPane.fxml"));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}

