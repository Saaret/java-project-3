package griddler.ui.javaFX_UI.mainScene;

/**
 * Created by Omri on 20/9/2016.
 */
class GriddlerStaticText {
    final static String WELCOME_TO_GRIDDLER = "Welcome to the Griddler Game!";
    final static String USER_ENDED_GAME = "User Has Ended the game!\nYou can peek at all the player boards in the score board!";
    final static String TURNS_ENDED_GAME = "Game has ended after all turns were used!\nYou can peek at all the player boards in the score board!";
}
