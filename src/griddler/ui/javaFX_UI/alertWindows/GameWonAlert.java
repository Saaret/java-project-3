package griddler.ui.javaFX_UI.alertWindows;

import griddler.logic.gameManager.gameRoom.Player;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class GameWonAlert {
    public static void showPlayerWonAlert(Player winningPlayer) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Player Won!!");
        alert.setHeaderText("Player " + winningPlayer.getName() + "Won!\n Hooray!");

        Optional<ButtonType> result = alert.showAndWait();
    }
}
