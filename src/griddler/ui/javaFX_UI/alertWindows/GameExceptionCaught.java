package griddler.ui.javaFX_UI.alertWindows;

import javafx.scene.control.Alert;

public class GameExceptionCaught {

    public static void GameExceptionAlertWindow(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error loading game");
        alert.setHeaderText("An exception was caught while attemping to load the game");
        alert.setContentText(message);

        alert.showAndWait();
    }
}
