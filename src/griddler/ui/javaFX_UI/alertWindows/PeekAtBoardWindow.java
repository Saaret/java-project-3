package griddler.ui.javaFX_UI.alertWindows;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.ui.javaFX_UI.controllers.UI_BoardGridPane;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUI_GameStaticParams;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class PeekAtBoardWindow {
    Stage stage;
    Label header;
    Button closeButton;
    BorderPane peekBoard;
    GridPane gameBoard;

    public PeekAtBoardWindow(GameRoom gameRoom, Player peekablePlayer) {

        initializeComponents(gameRoom, peekablePlayer);

        stage.setScene(new Scene(new ScrollPane(peekBoard), Screen.getPrimary().getVisualBounds().getWidth() / 3, Screen.getPrimary().getVisualBounds().getHeight() / 3));
        stage.getScene().getStylesheets().clear();
        stage.getScene().getStylesheets().add(getClass().getResource(GriddlerGUI_GameStaticParams.getCssResource()).toExternalForm());
        stage.showAndWait();
    }

    private void initializeComponents(GameRoom gameRoom, Player peekablePlayer) {
        stage = new Stage();
        stage.setTitle("You're Peeking!");
        stage.initModality(Modality.WINDOW_MODAL);

        gameBoard = createPeekedBoard(gameRoom, peekablePlayer);
        //Init layout
        peekBoard = new BorderPane();
        initHeader(peekablePlayer);
        initCloseButton();
        //init TextArea
        setPeekBoardLayout();

    }

    private void initHeader(Player peekablePlayer) {
        //Init header
        header = new Label("Peeking at " + peekablePlayer.getName() + "'s board!");
        header.setStyle("-fx-background-color: antiquewhite");
    }

    private void initCloseButton() {
        //Init button
        closeButton = new Button("Close");
        closeButton.setOnMouseClicked(e -> stage.close());
    }

    private void setPeekBoardLayout() {
        //Set the board
        peekBoard.setCenter(gameBoard);
        peekBoard.getCenter().setDisable(true);
        peekBoard.setTop(header);
        peekBoard.setBottom(closeButton);

    }

    /*
    We create a board according to the player sent
     */
    private GridPane createPeekedBoard(GameRoom gameRoom, Player peekablePlayer) {
        UI_BoardGridPane peekedBoard = new UI_BoardGridPane();
        peekedBoard.createGridPaneOfCells(gameRoom);
        peekedBoard.updateBoardGUICells(peekablePlayer.getPlayerBoard());
        return peekedBoard.getGridPane();
    }


}