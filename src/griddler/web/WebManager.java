package griddler.web;

import com.sun.xml.internal.bind.v2.util.StackRecorder;
import griddler.GameManager;
import griddler.logic.descriptor.GameDescriptor;
import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.gameRoom.Turn;
import griddler.logic.gameManager.gameRoom.board.Cell;
import griddler.logic.gameManager.gameRoom.board.Slices;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.logic.gameManager.logicEngine.boardValidations.BoardValidator;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerLogicException;
import griddler.web.jsonClasses.*;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Optional;

/**
 * Manages calls made by servlets to game classes
 */
public class WebManager {
    private static GameManager gameManager = new GameManager();
    private static JsonGameRoomList jsonGameRoomList = new JsonGameRoomList();

    /**
     * @param queryString from the game json sent
     * @param sessionID   generated from the tomcat session identifier
     * @param request
     * @return true if the player exists or false if not
     */
    public static boolean isPlayerInPlayerList(String queryString, String sessionID, HttpServletRequest request) {
        String[] params = queryString.split("&");
        String name = params[0].split("=")[1];
        boolean isHuman = params[1].split("=")[1].equals("true"), result;

        if (gameManager.isPlayerInCollection(name)) {
            result = true;
        } else {
            result = false;
            gameManager.addPlayerToUserNameCollection(name, isHuman, sessionID);
            request.getSession(true).setAttribute(ServletConstants.USER_NAME, name);
        }
        return result;
    }

    /**
     * We retrieve the players name list from the gameManager and create a json object
     *
     * @return the json as a string
     */
    public static String getFullPlayerListAsJson() {
        String[] playerNameList = gameManager.getPlayersNameList();
        JsonPlayerNameList jsonPlayerNameList = new JsonPlayerNameList(playerNameList);
        final ObjectMapper mapper = new ObjectMapper();
        String playerList = null;

        try {
            playerList = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonPlayerNameList);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return playerList;
    }

    /**
     * Receives a streamed xml file and attempts to load
     * If we fail an exception containing the error message is thrown
     *
     * @param xmlFile
     * @param uploaderName
     * @throws GriddlerLogicException
     * @throws FileNotFoundException
     * @throws JAXBException
     */
    public static void attemptGameLoad(Part xmlFile, String uploaderName) throws GriddlerLogicException, IOException, JAXBException {
        GameDescriptor gameDescriptor;
        JAXBContext jaxbContext = JAXBContext.newInstance(GameDescriptor.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        gameDescriptor = (GameDescriptor) jaxbUnmarshaller.unmarshal(xmlFile.getInputStream());

        //If no duplicate name exists we add the game or throw an exception
        if (!gameManager.isGameWithTitle(gameDescriptor.getDynamicMultiPlayers().getGametitle())) {
            BoardValidator boardValidator = new BoardValidator();
            GameRoom gameRoom = new GameRoom();
            if (boardValidator.isValidGameDescriptor(gameDescriptor)) {
                gameRoom.setFieldsFromGameDescriptor(gameDescriptor);
                gameRoom.setUploadedBy(uploaderName);
                gameManager.addNewGameRoom(gameRoom);
            }
        } else {
            throw new GriddlerLogicException("Game with duplicate title found, please change the game title and try again");
        }
    }

    /**
     * Returns a json of the success response
     *
     * @param b has the method succeeded or not
     * @return a string of the value or null
     */
    public static String returnSuccess(boolean b) {
        JsonSuccessResponse jsonSuccessResponse = new JsonSuccessResponse(b);
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonSuccessResponse);
        } catch (IOException e) {
            return null;
        }
    }

    public static String returnSuccess(boolean b, String message) {
        JsonSuccessResponse jsonSuccessResponse = new JsonSuccessResponse(b, message);
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonSuccessResponse);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Adds a new game room to the collection
     *
     * @param gameRoom
     */
    public static void addGameRoom(GameRoom gameRoom) {
        gameManager.getGameRoomList().add(gameRoom);
    }

    /**
     * Serializes the game room list ass a JSON and returns it
     *
     * @return
     */
    public static String getGameRoomListAsJSON() {
        JsonGameRoomList jsonGameRoomList = new JsonGameRoomList(gameManager.getGameRoomList());
        String json = null;
        final ObjectMapper mapper = new ObjectMapper();

        try {
            json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonGameRoomList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * @param gameTitle the name of the gameRoom we are initializing for the client
     * @param username
     * @return a null json string if the
     */
    public static String getGameRoomJsonForRequest(String gameTitle, String username) {
        final JsonGameRoomInitializer jsonGameRoomInitializer = new JsonGameRoomInitializer();
        final ObjectMapper objectMapper = new ObjectMapper();
        String json = "";

        GameRoom gameRoomToJsonify = gameManager.getGameByTitle(gameTitle);

        if (gameRoomToJsonify != null) {

            jsonGameRoomInitializer.updateJsonFields(gameRoomToJsonify, username);
            try {
                json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonGameRoomInitializer);
            } catch (IOException e) {
                json = returnSuccess(false, e.getMessage());
            }
        } else {
            json = returnSuccess(false, "Game Title not found!");
        }

        return json;
    }

    /**
     * Adds a player to a game room and returns the success status
     *
     * @param request
     * @param response
     */
    public static void addPlayerToGameRoom(HttpServletRequest request, HttpServletResponse response) {
        String gameTitle = request.getParameter(ServletConstants.GAME_TITLE);
        String userName = SessionUtils.getUsername(request);
        String errorMessage;
        try (PrintWriter out = response.getWriter()) {

            errorMessage = gameManager.addPlayerToGameRoom(userName, gameTitle);
            if (errorMessage == null) {
                out.println(returnSuccess(true));
            } else {
                out.println(returnSuccess(false, errorMessage));
            }

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Find the game room with the matching game title and create a json of it's players
     *
     * @param gameTitle
     * @return
     */
    public static String getInitialPlayerList(String gameTitle) {
        final ObjectMapper mapper = new ObjectMapper();
        String json = null;
        JsonPlayerNameWithTypeList jsonPlayerNameWithTypeList = new JsonPlayerNameWithTypeList(gameManager.getGameByTitle(gameTitle));

        try {
            json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonPlayerNameWithTypeList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * We check by game title if the game is in progress
     *
     * @param gameTitle
     * @return
     */
    public static String isGameInProgress(String gameTitle) {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);
        String json;

        if (gameRoom.isGameInProgress()) {
            json = "{\"isGameInProgress\": true }";
        } else {
            json = "{\"isGameInProgress\": false }";
        }

        return json;
    }

    /**
     * Find the gameRoom and return a json of the spectator list
     *
     * @param gameTitle
     * @return
     */
    public static String getSpectatorList(String gameTitle) {
        final ObjectMapper mapper = new ObjectMapper();
        String response = null;

        if (gameManager.isGameWithTitle(gameTitle)) {
            JsonSpectatorsList jsonSpectatorsList = new JsonSpectatorsList(gameManager.getGameByTitle(gameTitle).getSpectatorList());

            try {
                response = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonSpectatorsList);
            } catch (IOException e) {
                System.err.print(e.getMessage());
            }
        } else {
            response = returnSuccess(false, "Game Not found ");
        }

        return response;
    }

    /**
     * recives a game title and return a json of the players status and scores
     *
     * @param gameTitle the relevant game room
     * @return json of players name and scores
     */
    public static String getPlayerScores(String gameTitle) throws IOException {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);
        final ObjectMapper mapper = new ObjectMapper();
        HashMap<String, Integer> playerScoreMap = new HashMap<String, Integer>();

        for (Player player : gameRoom.getPlayersList()) {
            playerScoreMap.put(player.getName(), player.getStats().getScore());
        }
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(playerScoreMap);
    }

    /**
     * Retrieves the player from the game room and returns his initial stats
     * We first check that the game and the player are present
     *
     * @param gameTitle
     * @return
     */
    public static String getThisPlayerInitializer(String gameTitle, String userName) {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);
        Player playerToParse = null;
        String jsonResponse;
        if (gameRoom != null) {
            playerToParse = gameRoom.getPlayerByName(userName);
        } else {
            jsonResponse = returnSuccess(false, "Game Room not found by title " + gameTitle);
        }

        if (playerToParse != null) {
            jsonResponse = JsonPlayerNameAndType.getPlayerToJsonString(playerToParse, gameRoom.getCurrentNumberOfTurns());
        } else {
            jsonResponse = returnSuccess(false, "Player not found");
        }

        return jsonResponse;
    }

    /**
     * Removes the player from all gameRooms and the userName collection
     *
     * @param userName
     * @return the appropriate json response
     */
    public static String removePlayerFromGames(String userName) {
        String jsonResponse = null;
        try {

            for (GameRoom gameRoom : gameManager.getGameRoomList()) {
                WebManager.leaveGameRoom(gameRoom.getGameTitle(), userName);
            }
            gameManager.removePlayerToUserNameCollection(userName);
            jsonResponse = returnSuccess(true);
        } catch (RuntimeException e) {
            jsonResponse = returnSuccess(false, e.getMessage());
        }
        return jsonResponse;
    }

    /**
     * Query who the current player in the game is
     * If the game hasn't begun, return failure response, else return the player
     *
     * @param gameTitle
     * @return
     */
    public static String getCurrentPlayer(String gameTitle) {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);
        return gameRoom.isGameInProgress()
                ?
                JsonPlayerNameAndType.getPlayerToJsonString(gameRoom.getCurrentPlayer(), gameRoom.getCurrentNumberOfTurns())
                :
                returnSuccess(false, "Game has not begun yet, no current player");
    }

    /**
     * Removes a player from a game room
     *
     * @param gameTitle the requesting gameRoom
     * @param username  the user to remove
     * @return status message
     */
    public static String leaveGameRoom(String gameTitle, String username) {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);
        String json = "";
        try {
            if (gameRoom.getPlayerByName(username) != null) {
                Player playerToRemove = gameRoom.getPlayerByName(username);

                if (playerToRemove == gameRoom.getCurrentPlayer()) {
                    gameRoom.advanceToNextPlayer();
                }

                gameRoom.getPlayersList().remove(playerToRemove);

                //Handle technical win
                if (gameRoom.getPlayersList().size() == 1 && !gameRoom.isSinglePlayerGame()) {
                    gameRoom.setGameInProgress(false);
                    gameRoom.setGameFrozen(true);
                    gameRoom.setWinningPlayer(gameRoom.getPlayersList().get(0).getName());
                } else if (gameRoom.getPlayersList().size() == 0) {
                    gameRoom.setGameInProgress(false);
                    gameRoom.resetGameRoom();
                }
            } else if (gameRoom.getSpectatorByName(username) != null) {
                Player spectatorToRemove = gameRoom.getSpectatorByName(username);
                gameRoom.getSpectatorList().remove(spectatorToRemove);

            }
            json = returnSuccess(true);
        } catch (RuntimeException e) {
            json = returnSuccess(false, e.getMessage());
        }
        return json;
    }

    /**
     * Submit's a move and updates the player's board
     *
     * @param gameTitle relevant game room title
     * @param request   the corresponding HttpRequest
     * @return an updated collection of gameBoard Slices
     */
    public static String submitMove(String gameTitle, HttpServletRequest request) throws Exception {
        String turnJson = request.getParameter(ServletConstants.GAME_MOVE), playerName = SessionUtils.getUsername(request);
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);
        Player player = gameRoom.getPlayerByName(playerName);
        final ObjectMapper mapper = new ObjectMapper();
        JsonTurn jsonTurnToPerform = mapper.readValue(turnJson, JsonTurn.class);
        Turn turnToPerform = Turn.parseFromJson(jsonTurnToPerform);

        player.performTurn(turnToPerform);

        if (player.getStats().getScore() == 100) {
            gameRoom.setWinningPlayer(player.getName());
            gameRoom.setGameInProgress(false);
            gameRoom.setGameFrozen(true);
        }

        gameRoom.runPerfectBlockFinder();

        return createPerfectBlocksJson(gameRoom.getColSlices(), gameRoom.getRowSlices());
    }

    /**
     * @param colSlices for a gameBoard
     * @param rowSlices for a gameBoard
     * @return a json of both
     */
    private static String createPerfectBlocksJson(Slices colSlices, Slices rowSlices) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        JsonBoardBlocks jsonBoardBlocks = new JsonBoardBlocks(colSlices, rowSlices);

        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonBoardBlocks);
    }

    /**
     * @param gameTitle relevant game
     * @param username  relevant player
     * @return json of the current board
     */
    public static String getBoard(String gameTitle, String username) {
        final ObjectMapper mapper = new ObjectMapper();
        Cell[][] gameBoard = gameManager.getGameByTitle(gameTitle).getPlayerByName(username).getPlayerBoard();

        return JsonGameBoard.getGameBoardAsJson(gameBoard);
    }

    /**
     * Handles updating and parsing updated blocks for our
     *
     * @param gameTitle
     * @param username
     * @return
     */
    public static String getSlices(String gameTitle, String username) throws Exception {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);

        gameRoom.runPerfectBlockFinder();

        return createPerfectBlocksJson(gameRoom.getColSlices(), gameRoom.getRowSlices());
    }

    /**
     * Handles submitting a turn end and advancing to the next player
     * Also alters the "isGameInProgress" so we know that the game is won
     *
     * @param gameTitle
     * @return
     */
    public static String endTurn(String gameTitle) {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);

        if (gameRoom.getCurrentPlayer().getStats().getScore() == 100) {
            gameRoom.setGameInProgress(false);
            gameRoom.setWinningPlayer(gameRoom.getCurrentPlayer().getName());
        } else {
            gameRoom.advanceToNextPlayer();
        }

        return returnSuccess(true);
    }

    /**
     * Generates and performs a AI turn
     *
     * @param gameTitle
     * @param username
     * @return success status
     */
    public static String performAI_Turn(String gameTitle, String username) {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);
        Player player = gameRoom.getPlayerByName(username);
        player.performTurn(LogicEngine.getAITurn(gameRoom.getBoardCols(), gameRoom.getBoardRows()));

        return returnSuccess(true);
    }

    /**
     * Checks if the game has ended
     * 1. Check if the game is still in progress: true -> return that the game hasn't ended : false-> continue to step 2
     * 2. Check if a winning player has been selected
     * 3. true -> check if he won by score or technicality
     * 4. false -> conclude that the turns ran out
     * 5. Parse json response and return
     *
     * @param gameTitle
     * @return json with parsed response
     */
    public static String isGameEnded(String gameTitle) {
        GameRoom gameRoom = gameManager.getGameByTitle(gameTitle);
        String json;

        if (!gameRoom.isGameInProgress()) {
            if (gameRoom.getWinningPlayer() != null) {
                if (gameRoom.getPlayerByName(gameRoom.getWinningPlayer()).getStats().getScore() == 100) {
                    json = returnSuccess(true, "Player " + gameRoom.getWinningPlayer() + " has won with a 100 score!");
                } else {
                    json = returnSuccess(true, "Player " + gameRoom.getWinningPlayer() + " has won by technicality!");
                }

            } else {
                json = returnSuccess(true, "Game ended by reaching turn limit!");
            }
        } else {
            json = returnSuccess(false);
        }

        return json;
    }

    /**
     * Receive a game room + session requesting an undo to be performed
     *
     * @param gameTitle
     * @param request
     * @return json with status
     */
    public static String performUndo(String gameTitle, HttpServletRequest request) {
        Player player = gameManager.getGameByTitle(gameTitle).getPlayerByName(SessionUtils.getUsername(request));
        int numOfTurns = player.getTurnList().size();
        if (player.getTurnList().size() == 0) {
            return WebManager.returnSuccess(false, "0");
        } else {
            player.performUndo();
            return WebManager.returnSuccess(true, numOfTurns - 1 + "");
        }
    }

    /**
     * Return the relevant game room
     *
     * @param gameTitle name of the game room being queried
     * @return json with board or success response
     */
    public static String getCurrentPlayerBoard(String gameTitle) {
        String json = "";

        if (!gameManager.getGameByTitle(gameTitle).getCurrentPlayer().isInReplay()) {
            Cell[][] currentPlayerBoard = gameManager.getGameByTitle(gameTitle).getCurrentPlayer().getPlayerBoard();
            try {
                JsonGameBoard jsonGameBoard = new JsonGameBoard(currentPlayerBoard);
                json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(jsonGameBoard);
            } catch (Exception e) {
                json = returnSuccess(false, "Error in getting current player board");
            }
        } else {
            json = returnSuccess(true, "Current player in replay mode");
        }

        return json;
    }

    /**
     * Notify that the player is beginning a replay session
     * Create a "return to point" for ending the undo
     *
     * @param gameTitle the game room
     * @param username  the player
     * @return status that replay session has begun successfully
     */
    public static String beginPlayerReplay(String gameTitle, String username) {
        String json = "";
        HashMap<String, Integer> turnStatus = new HashMap<>();
        try {
            Player playerBeginningReplay = gameManager.getGameByTitle(gameTitle).getPlayerByName(username);
            playerBeginningReplay.setTurnBookMarkForReplay(playerBeginningReplay.getTurnList().getLast());
            turnStatus.put("remainingUndo", playerBeginningReplay.getTurnList().size());
            turnStatus.put("remainingRedo", playerBeginningReplay.getRedoList().size());
            json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(turnStatus);
        } catch (Exception e) {
            json = returnSuccess(false, "Error in WebManager.beginPlayerReplay");
        }

        return json;
    }

    /**
     * Handles restoring the player's board and move lists to normal play session
     *
     * @param gameTitle the game room
     * @param username  the player
     * @return status that replay session has ended successfully and the board has been restored
     */
    public static String endPlayerReplay(String gameTitle, String username) {
        String json = "";
        Player player = gameManager.getGameByTitle(gameTitle).getPlayerByName(username);
        try {
            player.restoreMoveListAfterReplay();
            json = returnSuccess(true);
        } catch (Exception e) {
            json = returnSuccess(false, "Error in WebManager.endPlayerReplay");
        }

        return json;
    }

    /**
     * Performs a redo for the player, and return the number of remaining redo
     *
     * @param gameTitle
     * @param username
     * @return
     */
    public static String performRedo(String gameTitle, String username) {
        Player player = gameManager.getGameByTitle(gameTitle).getPlayerByName(username);
        String json = "";
        if (player.isRedoListEmpty()) {
            json = returnSuccess(false, 0 + "");
        } else {
            player.performRedo();
            json = returnSuccess(true, player.getRedoList().size() + "");
        }
        return json;
    }
}
