package griddler.web.servlets;

import griddler.web.ServletConstants;
import griddler.web.SessionUtils;
import griddler.web.WebManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet handling all game room requests
 */
public class GameRoomServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String requestType = request.getParameter(ServletConstants.REQUEST_TYPE);
        String gameTitle = request.getParameter(ServletConstants.GAME_TITLE);

        PrintWriter out = response.getWriter();
        try {
            switch (requestType) {
                case ServletConstants.INIT_ROOM:
                    out.println(WebManager.getGameRoomJsonForRequest(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.INIT_PLAYERS:
                    out.println(WebManager.getInitialPlayerList(gameTitle));
                    break;
                case ServletConstants.INIT_THIS_PLAYER:
                    out.println(WebManager.getThisPlayerInitializer(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.DID_GAME_START:
                    out.println(WebManager.isGameInProgress(gameTitle));
                    break;
                case ServletConstants.GET_SPECTATORS:
                    out.println(WebManager.getSpectatorList(gameTitle));
                    break;
                case ServletConstants.CURRENT_PLAYER:
                    out.println(WebManager.getCurrentPlayer(gameTitle));
                    break;
                case ServletConstants.SUBMIT_MOVE:
                    out.println(WebManager.submitMove(gameTitle, request));
                    break;
                case ServletConstants.PERFORM_UNDO:
                    out.println(WebManager.performUndo(gameTitle, request));
                    break;
                case ServletConstants.LEAVE_GAME:
                    out.println(WebManager.leaveGameRoom(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.GET_BOARD:
                    out.println(WebManager.getBoard(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.GET_CURRENT_PLAYER_BOARD:
                    out.println(WebManager.getCurrentPlayerBoard(gameTitle));
                    break;
                case ServletConstants.GET_SLICES:
                    out.println(WebManager.getSlices(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.PLAYER_SCORES:
                    out.println(WebManager.getPlayerScores(gameTitle));
                    break;
                case ServletConstants.BEGIN_REPLAY:
                    out.println(WebManager.beginPlayerReplay(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.END_REPLAY:
                    out.println(WebManager.endPlayerReplay(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.PERFORM_REDO:
                    out.println(WebManager.performRedo(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.END_TURN:
                    out.println(WebManager.endTurn(gameTitle));
                    break;
                case ServletConstants.AI_TURN:
                    out.println(WebManager.performAI_Turn(gameTitle, SessionUtils.getUsername(request)));
                    break;
                case ServletConstants.IS_GAME_WON:
                    out.println(WebManager.isGameEnded(gameTitle));
                    break;

            }
        } catch (Exception e) {
            out.println(WebManager.returnSuccess(false, requestType + " failed!"));
        }
        out.flush();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Passes info to WebManager to login player";
    }// </editor-fold>
}
