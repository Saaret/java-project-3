package griddler.web.servlets;

import griddler.logic.descriptor.GameDescriptor;
import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerGameNotFoundException;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerLogicException;
import griddler.web.ServletConstants;
import griddler.web.WebManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.Part;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Handles opening a input stream for loading the xml game files
 */
@MultipartConfig
public class XmlUploadHandlerServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        Part xmlFile = request.getPart("xmlFile");
        PrintWriter out = response.getWriter();
        synchronized (this) {
            try {
                WebManager.attemptGameLoad(xmlFile,(String)request.getSession(true).getAttribute(ServletConstants.USER_NAME));
                out.println(WebManager.returnSuccess(true));
            } catch (GriddlerLogicException | JAXBException e) {
                out.println(WebManager.returnSuccess(false, e.getMessage()));
            } catch (NullPointerException e) {
                out.println(WebManager.returnSuccess(false, "Illegal file used! (Wrong format for web game?)"));
            } finally {
                out.flush();
            }
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Passes info to WebManager to login player";
    }// </editor-fold>
}
