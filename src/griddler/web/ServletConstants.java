package griddler.web;

/**
 * For aggregating the servlet constants
 */
public class ServletConstants {
    public static final String USER_NAME = "userName";
    public static final String REQUEST_TYPE = "RequestType";
    public static final String INIT_ROOM = "InitRoom";
    public static final String GAME_TITLE = "GameTitle";

    public static final String INIT_PLAYERS = "GetInitPlayers";
    public static final String DID_GAME_START = "DidGameStart";
    public static final String GET_SPECTATORS = "GetSpectators";
    public static final String PLAYER_SCORES = "PlayerScores";
    public static final String INIT_THIS_PLAYER = "InitThisPlayer";
    public static final String CURRENT_PLAYER = "GetCurrentPlayer";
    public static final String LEAVE_GAME = "LeaveGame";
    public static final String SUBMIT_MOVE = "MoveSubmit";
    public static final String GAME_MOVE = "GameMove";
    public static final String GET_BOARD = "getBoard";
    public static final String GET_CURRENT_PLAYER_BOARD = "GetCurrentPlayerBoard";
    public static final String GET_SLICES = "getSlices";
    public static final String END_TURN = "EndTurn";
    public static final String AI_TURN = "AITurn";
    public static final String IS_GAME_WON = "IsGameWon";
    public static final String PERFORM_UNDO = "PerformUndo";
    public static final String BEGIN_REPLAY = "BeginReplay";
    public static final String END_REPLAY = "EndReplay";
    public static final String PERFORM_REDO = "PerformRedo";
}
