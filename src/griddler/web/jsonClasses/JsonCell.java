package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.board.Cell;
import griddler.logic.gameManager.gameRoom.board.CellStatus;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Serializes a single cell to json
 */
public class JsonCell {
    @JsonProperty("X")
    public int x;
    @JsonProperty("Y")
    public int y;
    @JsonProperty("CellStatus")
    public CellStatus cellStatus;

    public JsonCell(Cell cell) {
        x = cell.CellToPoint().x;
        y = cell.CellToPoint().y;
        cellStatus = cell.getCellStatus();
    }
}
