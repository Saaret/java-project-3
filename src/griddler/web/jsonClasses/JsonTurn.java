package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.board.CellStatus;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Serializes a move for transmitting
 */
public class JsonTurn {
    @JsonProperty("moveColor")
    public CellStatus moveColor;
    @JsonProperty("chosenCells")
    public List<JsonPoint> chosenCells;
}
