package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.GameRoom;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Omri on 19/10/2016.
 */
public class JsonGameRoom {
    @JsonProperty("gameName")
    public String gameName;
    @JsonProperty("uploaderName")
    public String uploaderName;
    @JsonProperty("maxMovesNumber")
    public int maxMovesNumber;
    @JsonProperty("boardColSize")
    public int boardColSize;
    @JsonProperty("boardRowSize")
    public int boardRowSize;
    @JsonProperty("maxNumberOfPlayers")
    public int maxNumberOfPlayers;
    @JsonProperty("currentNumberOfPlayers")
    public int currentNumberOfPlayers;
    @JsonProperty("currentNumberOfSpectators")
    public int currentNumOfSpecs;

    public JsonGameRoom(GameRoom gameRoom) {
        gameName = gameRoom.getGameTitle();
        uploaderName = gameRoom.getUploadedBy();
        maxMovesNumber = gameRoom.getMaxNumberOfTurns();
        boardColSize = gameRoom.getBoardCols();
        boardRowSize = gameRoom.getBoardRows();
        maxNumberOfPlayers = gameRoom.getMaxNumberOfPlayers();
        currentNumberOfPlayers = gameRoom.getCurrentNumberOfPlayers();
        currentNumOfSpecs = gameRoom.getSpectatorList().size();
    }
}
