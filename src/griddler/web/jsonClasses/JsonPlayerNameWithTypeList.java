package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.Player;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Omri on 21/10/2016.
 */
public class JsonPlayerNameWithTypeList {
    @JsonProperty("playerList")
    public List<JsonPlayerNameAndType> jsonPlayerNameAndTypeList;

    /**
     * Constructs a serialized json for our player list status
     *
     * @param gameRoom
     */
    public JsonPlayerNameWithTypeList(GameRoom gameRoom) {
        int size = gameRoom.getPlayersList().size();
        jsonPlayerNameAndTypeList = new LinkedList<>();

        for (int i = 0; i < size; i++) {
            jsonPlayerNameAndTypeList.add(new JsonPlayerNameAndType(gameRoom.getPlayersList().get(i), gameRoom.getCurrentNumberOfTurns()));
        }
    }
}
