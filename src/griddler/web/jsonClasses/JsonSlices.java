package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.board.Slice;
import griddler.logic.gameManager.gameRoom.board.Slices;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Serializes a list of slices
 */
public class JsonSlices {
    @JsonProperty("SliceList")
    public List<JsonSlice> sliceList;

    public JsonSlices(Slices rowSlices) {
        sliceList = new ArrayList<>(rowSlices.getSliceList().size());
        int index = 0;
        for (Slice slice : rowSlices.getSliceList()) {
            sliceList.add(new JsonSlice(slice));
        }
    }
}
