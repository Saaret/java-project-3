package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.Player;
import griddler.web.WebManager;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Serializes a player
 */
public class JsonPlayerNameAndType {
    @JsonProperty("PlayerName")
    public String playerName;
    @JsonProperty("IsComputer")
    public boolean isComputerPlayer;
    @JsonProperty("IsSpectator")
    public boolean isSpectator;
    @JsonProperty("Score")
    public int score;
    @JsonProperty("CurrentTurn")
    public int currentTurn;

    public JsonPlayerNameAndType() {
    }

    public JsonPlayerNameAndType(Player player, int currentTurn) {
        playerName = player.getName();
        isComputerPlayer = player.isComputerPlayer();
        this.currentTurn = currentTurn;

        if (player.getStats() == null || player.getPlayerBoard() == null) {
            score = -1;
        } else {
            score = player.getStats().getScore();
        }
    }

    /**
     * Creates a json to return from provided player
     *
     * @param player
     * @return
     */
    public static String getPlayerToJsonString(Player player, int currentTurn) {
        JsonPlayerNameAndType jsonPlayerNameAndType = new JsonPlayerNameAndType(player, currentTurn);
        final ObjectMapper mapper = new ObjectMapper();
        String jsonResponse;

        try {
            jsonResponse = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonPlayerNameAndType);
        } catch (IOException e) {
            jsonResponse = WebManager.returnSuccess(false, e.getMessage());
        }

        return jsonResponse;
    }
}
