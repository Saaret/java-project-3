package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.board.Slices;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Serialized updated slices
 */
public class JsonBoardBlocks {
    @JsonProperty("RowSlices")
    JsonSlices rowSlices;
    @JsonProperty("ColSlices")
    JsonSlices colSlices;

    public JsonBoardBlocks(Slices colSlices, Slices rowSlices) {
        this.rowSlices = new JsonSlices(rowSlices);
        this.colSlices = new JsonSlices(colSlices);;
    }
}
