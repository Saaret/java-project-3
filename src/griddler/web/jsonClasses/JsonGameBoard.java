package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.board.Cell;
import griddler.web.WebManager;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Serializes a 2d board of cells
 */
public class JsonGameBoard {
    @JsonProperty("GameBoard")
    JsonCell[][] gameBoard;

    public JsonGameBoard(Cell[][] gameCellArray) {
        gameBoard = new JsonCell[gameCellArray.length][gameCellArray[0].length];
        for (int i = 0; i < gameCellArray[0].length; i++) {
            for (int j = 0; j < gameCellArray.length; j++) {
                gameBoard[j][i] = new JsonCell(gameCellArray[j][i]);
            }
        }
    }

    /**
     * Receives a 2d game board and serializes it as a json
     *
     * @param gameCellArray
     * @return
     */
    public static String getGameBoardAsJson(Cell[][] gameCellArray) {
        final ObjectMapper mapper = new ObjectMapper();
        JsonGameBoard jsonGameBoard = new JsonGameBoard(gameCellArray);
        String json;
        try {
            json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonGameBoard);
        } catch (IOException e) {
            json = WebManager.returnSuccess(false, e.getMessage());
        }

        return json;
    }

}
