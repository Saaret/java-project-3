package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.GameRoom;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Aggregates and serializes the response for initializing a new gameRoom for a client
 */
public class JsonGameRoomInitializer {
    @JsonProperty("GameTitle")
    public String gameTitle;
    @JsonProperty("MaxTurns")
    public int maxTurns;
    @JsonProperty("ColNumber")
    public int colNum;
    @JsonProperty("RowNumber")
    public int rowNum;
    @JsonProperty("RowSlices")
    public JsonSlices rowSlices;
    @JsonProperty("ColSlices")
    public JsonSlices colSlices;
    @JsonProperty("WhoAreYou")
    public String whoAreYou;
    @JsonProperty("isPlayer")
    public boolean isPlayer;

    public JsonGameRoomInitializer() {
    }

    /**
     * Updates the objects fields according to the game room we received
     *
     * @param gameRoomToJsonify
     * @param username
     */
    public void updateJsonFields(GameRoom gameRoomToJsonify, String username) {
        gameTitle = gameRoomToJsonify.getGameTitle();
        maxTurns = gameRoomToJsonify.getMaxNumberOfTurns();
        colNum = gameRoomToJsonify.getBoardCols();
        rowNum = gameRoomToJsonify.getBoardRows();
        rowSlices = new JsonSlices(gameRoomToJsonify.getRowSlices());
        colSlices = new JsonSlices(gameRoomToJsonify.getColSlices());
        whoAreYou = username;
        isPlayer = gameRoomToJsonify.getPlayerByName(username) != null && !gameRoomToJsonify.getPlayerByName(username).isSpectator();
    }
}
