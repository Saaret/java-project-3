package griddler.web.jsonClasses;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.LinkedList;

/**
 * For parsing a simple success or error list response
 */
public class JsonSuccessResponse {
    @JsonProperty("success")
    public boolean response;
    @JsonProperty("message")
    public String messageList;

    public JsonSuccessResponse(boolean response) {
        this.response = response;
    }

    public JsonSuccessResponse(boolean response, String message) {
        this.response = response;
        this.messageList = message;
    }
}
