package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.Player;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Aggregates a list of player names
 */
public class JsonPlayerNameList {
    @JsonProperty("playerName")
    public String[] playerNameArray;

    public JsonPlayerNameList(String[] playerNameList) {
        this.playerNameArray = playerNameList;
    }

    /**
     * Recives a List<Player> and converts to array and serializes it
     * if the list is empty return null
     */
    public JsonPlayerNameList(List<Player> playerNameList) {
        if (playerNameList != null) {
            playerNameArray = new String[playerNameList.size()];
            int index = 0;
            for (String playerName : playerNameArray) {
                playerName = playerNameList.get(index++).getName();
            }
        } else {
            playerNameArray = null;
        }
    }
}
