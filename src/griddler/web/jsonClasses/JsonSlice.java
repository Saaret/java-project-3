package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.board.Block;
import griddler.logic.gameManager.gameRoom.board.Slice;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Omri on 21/10/2016.
 */
public class JsonSlice {
    public List<JsonBlock> jsonBlocksList;
    @JsonProperty("SliceID")
    public int id;

    public JsonSlice(Slice slice) {
        int index = 0;
        id = slice.getId();
        jsonBlocksList = new ArrayList<>(slice.getSize());

        for (Block block : slice.getBlocks()) {
            jsonBlocksList.add(new JsonBlock(block));
        }
    }
}
