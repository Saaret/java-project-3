package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.GameRoom;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.LinkedList;
import java.util.List;

/**
 * Helps us serialize a game room list
 */
public class JsonGameRoomList {
    @JsonProperty("GameRoomList")
    List<JsonGameRoom> jsonGameRooms;

    public JsonGameRoomList(List<GameRoom> gameRooms) {
        LinkedList<JsonGameRoom> jsonGameRooms = new LinkedList<JsonGameRoom>();
        for (GameRoom gameRoom : gameRooms) {
            jsonGameRooms.add(new JsonGameRoom(gameRoom));
        }

        this.jsonGameRooms = jsonGameRooms;
    }

    public JsonGameRoomList() {
        LinkedList<JsonGameRoom> jsonGameRooms = new LinkedList<JsonGameRoom>();
    }

    public List<JsonGameRoom> getJsonGameRooms() {
        return jsonGameRooms;
    }
}
