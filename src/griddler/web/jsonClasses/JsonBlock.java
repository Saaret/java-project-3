package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.board.Block;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Omri on 21/10/2016.
 */
public class JsonBlock {
    @JsonProperty("BlockSize")
    int blockSize;
    @JsonProperty("IsPerfect")
    boolean isPerfect;

    public JsonBlock(Block block) {
        blockSize = block.getBlock();
        isPerfect = block.isPerfect();
    }
}
