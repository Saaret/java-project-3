package griddler.web.jsonClasses;

import griddler.logic.gameManager.gameRoom.Player;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Omri on 22/10/2016.
 */
public class JsonSpectatorsList {
    @JsonProperty("spectatorsList")
    public List<String> spectatorNameList;
    @JsonProperty("currentNumberOfSpectators")
    public int numOfSpectators;

    /**
     * Receives a List<Player> and converts to array and serializes it
     * if the list is empty return null
     */
    public JsonSpectatorsList(List<Player> toSerialize) {

        if (toSerialize != null && toSerialize.size() > 0) {
            spectatorNameList = new LinkedList<String>();
            int index = 0;
            for (Player player : toSerialize) {
                spectatorNameList.add(player.getName());
            }
            numOfSpectators = spectatorNameList.size();
        } else {
            spectatorNameList = null;
            numOfSpectators = 0;

        }
    }
}
