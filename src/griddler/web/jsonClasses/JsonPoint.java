package griddler.web.jsonClasses;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Serializes a point!
 */
public class JsonPoint {
    @JsonProperty("x")
    public int x;
    @JsonProperty("y")
    public int y;
}
