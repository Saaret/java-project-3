/**
 * Created by Saar on 22/10/2016.
 */

var xhrGetBoard = new XMLHttpRequest;

function setHeader() {
    $("#gameName").text(getGameTitle());
}

function getBoard() {
    // open the connection
    xhrGetBoard.open('POST', 'GameRoomQueries', true);
    xhrGetBoard.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var gameTitle = getGameTitle();
    var params = "RequestType=InitRoom&GameTitle=" + gameTitle;
    xhrGetBoard.send(params);
}

function getGameTitle() {
    var query = window.location.search.substring(1);
    return decodeURI(query).split('=')[1];
}

xhrGetBoard.onload = function(){
    if (xhrGetBoard.status === 200) {
        console.log("Successfully received game details for board preview");
        var gameDetails = JSON.parse(xhrGetBoard.responseText);
        initBoard(gameDetails);
    } else {
        console.log("Didn't receive game details for board preview");
    }
}

function initBoard(gameDetails) {
    $("#gameBoardDiv").empty();
    var numOfRows = gameDetails.RowNumber;
    var numOfCols = gameDetails.ColNumber;
    var rowSlices = gameDetails.RowSlices;
    var colSlices = gameDetails.ColSlices;
    var boardAsHTML = generateBoard(numOfRows, numOfCols, rowSlices, colSlices);
    $("#gameBoardDiv").html(boardAsHTML);
}

function returnLobby() {
    window.location.href = '../Griddler_Game/lobby.html';
}

$(window).on('load', function(){
    setHeader();
    getBoard();
});