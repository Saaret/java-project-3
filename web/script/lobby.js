/**
 * Created by Saar on 08/10/2016.
 */

var form;
var fileSelect;
var uploadButton;
var file;
var xhrUploadXml = new XMLHttpRequest;
var xhrJoinGame = new XMLHttpRequest;
var xhrLogOut = new XMLHttpRequest;
var refreshRate = 2000; //milliseconds

var gameIndex = 1;

function xmlUpload(event) {
    console.log("Uploading");
    uploadButton.innerHTML = 'Uploading';
    event.stopPropagation();
    event.preventDefault();
    // get the file
    file = fileSelect.files[0];
    console.log(file.name);
    // create a new FormData object
    var formData = new FormData();
    // add the file to the request
    formData.append('xmlFile', file);
    // open the connection
    xhrUploadXml.open('POST', 'xmlUploadHandler', true);
    // send the Data
    xhrUploadXml.send(formData);
}

function onFileChanged(fileSelect) {
    $("#upload-button").prop('disabled', fileSelect.files.length == 0);
}

// Set up a handler for when the request finishes.
xhrUploadXml.onload = function () {
    if (xhrUploadXml.status === 200) {
        // file uploaded
        var xmlValidation = JSON.parse(xhrUploadXml.responseText);
        if (xmlValidation.success == true) {
            console.log("A valid game successfully uploaded");
            alert("Game successfully uploaded!");
        } else if (xmlValidation.success == false) {
            console.log("An invalid game successfully uploaded");
            alert(xmlValidation.message);
        } else {
            console.log("A file was uploaded, the result is undefined");
        }
    } else {
        console.log("Game was not uploaded, probably because of a server problem");
        alert('Game was not uploaded!');
    }

    uploadButton.innerHTML = 'Upload';
}

function ajaxPlayersList() {
    $.ajax({
        url: "playersList",
        success: function (players) {
            refreshPlayersList(players);
        }
    });
}

//players = a list of usernames
function refreshPlayersList(players) {
    //clear all current users
    $("#playersList").empty();
    var allPlayers = players.playerName;
    $.each(allPlayers || [], function (index, username) {
        //create a new <option> tag with a value in it and
        //appeand it to the #userslist (div with id=userslist) element
        $('<li>' + decodeURI(username) + '</li>').appendTo($("#playersList"));
    });
}

function ajaxGameRoomsList() {
    $.ajax({
        url: "gameRoomList",
        success: function (gamesFromServer) {
            refreshGameRoomsList(gamesFromServer);
        }
    });
}

//players = a list of usernames
function refreshGameRoomsList(gamesFromServer) {
    //clear all current users
    if ($('#gameRoomsList li').length > 0) {
        $('#gameRoomsList').empty();
    }
    if (typeof gamesFromServer != 'undefined') {
        var numOfGameRooms = gamesFromServer.GameRoomList.length;
        if (numOfGameRooms > 0) {
            var listOfGameRoomsFromServer = gamesFromServer.GameRoomList;
            $.each(listOfGameRoomsFromServer, function (index, gameRoom) {
                //create a new <option> tag with a value in it and
                //appeand it to the #userslist (div with id=userslist) element
                $(
                    "<li>"
                    + "<div class=\"panel panel-default gameRoom deep\">"
                    + "<h4>" + gameRoom.gameName + "</h4><br>"
                    + "<b>Uploaded by:</b> " + decodeURI(gameRoom.uploaderName) + "<br>"
                    + "<b>Max number of moves:</b> " + gameRoom.maxMovesNumber + "<br>"
                    + "<b>Board size:</b> " + gameRoom.boardColSize + "x" + gameRoom.boardRowSize + "<br>"
                    + "<b>Number of players in the room:</b> " + gameRoom.currentNumberOfPlayers + "/" + gameRoom.maxNumberOfPlayers + "<br>"
                    + "<b>Number of spectators in the room:</b> " + gameRoom.currentNumberOfSpectators + "<br>"
                    + "<button class=\"btn btn-info\" id=\"" + gameRoom.gameName + "\" onclick=\"joinGameRoom(this.id)\">Enter Game Room</button><br>"
                    + "<button class=\"btn btn-info\" id=\"previewBoard." + gameRoom.gameName + "\" onclick=\"previewBoard(this.id)\">Preview Board</button>"
                    + "</div>"
                    + "</li>"
                ).appendTo($("#gameRoomsList"));

                gameIndex++;
            });
        }
    }
}

function joinGameRoom(gameName) {
    // open the connection
    xhrJoinGame.open('POST', 'joinGameRoom', true);
    xhrJoinGame.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "GameTitle=" + gameName;
    xhrJoinGame.send(params);
    xhrJoinGame.onload = function () {
        if (xhrJoinGame.status === 200) {
            // file uploaded
            var xmlValidation = JSON.parse(xhrJoinGame.responseText);
            if (xmlValidation.success == true) {
                console.log("Server responded, redirecting to game room");
                window.location.href = '../Griddler_Game/gameRoom.html?gameTitle=' + gameName;
            } else if (xmlValidation.success == false) {
                console.log("Server responded, not redirecting to game room");
            } else {
                console.log("The response result for joining game room is undefined");
            }
        } else {
            console.log("Server did not get the request to join room");
            alert('Failed joining game room!');
        }
    }
}

function previewBoard(buttonId) {
    var gameTitle = buttonId.split('.')[1];
    window.location.href = '../Griddler_Game/boardPreview.html?gameTitle=' + gameTitle;
}

function logOut() {
    // open the connection
    xhrLogOut.open('POST', 'LogOut', true);
    xhrLogOut.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    xhrLogOut.send();
    window.location.href = '../Griddler_Game/index.html';
}

xhrLogOut.onload = function () {
    if (xhrLogOut.status === 200) {
        console.log("Player has successfully logged out");
    } else {
        console.log("The player didn't log out successfully");
    }
}

$(window).on('load', function () {
    $("#file-form").on('submit', xmlUpload);
    console.log("page loaded");
    form = document.getElementById('file-form');
    fileSelect = document.getElementById('file-select');
    uploadButton = document.getElementById('upload-button');
    // the user list is refreshed automatically every second
    ajaxPlayersList();
    ajaxGameRoomsList();
    setInterval(ajaxPlayersList, refreshRate);
    setInterval(ajaxGameRoomsList, refreshRate);
});