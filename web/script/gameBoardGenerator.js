/**
 * Created by Saar on 21/10/2016.
 */

function generateBoard(numOfRows, numOfCols, RowSlices, ColSlices) {
    var boardAsHTML = "";
    for(var i = 0; i < numOfRows; i++) {
        for(var j = 0; j < numOfCols; j++) {
            boardAsHTML = boardAsHTML.concat("<label id=\"GameCellRow" + i + "Column" + j + "\" class=\"gameCell btn\" disabled></label> ");
        }
        var currentRowSlice = RowSlices.SliceList[i].jsonBlocksList;
        var numOfBlocks = currentRowSlice.length;
        for(var j = 0; j < numOfBlocks; j++) {
            var perfectSlice = currentRowSlice[j].IsPerfect == false ? "" : " perfectSlice";
            boardAsHTML = boardAsHTML.concat("<label id=\"RowSliceRow" + i + "Column" + j +"\" class=\"slice" + perfectSlice + "\">" + currentRowSlice[j].BlockSize + "</label>");
        }

        boardAsHTML = boardAsHTML.concat("<br>");
    }

    var longestSlice = 0;

    for(var i = 0; i < ColSlices.SliceList.length; i++) {
        if(ColSlices.SliceList[i].jsonBlocksList.length > longestSlice) {
            longestSlice = ColSlices.SliceList[i].jsonBlocksList.length;
        }
    }

    for(var i = 0; i < longestSlice; i++) {
        for(var j = 0; j < numOfCols; j++) {
            if(ColSlices.SliceList[j].jsonBlocksList.length > i) {
                var perfectSlice = ColSlices.SliceList[j].jsonBlocksList[i].IsPerfect == false ? "" : " perfectSlice";
                boardAsHTML = boardAsHTML.concat("<label id=\"ColSliceRow" + i + "Column" + j + "\" class=\"slice" + perfectSlice + "\">" + ColSlices.SliceList[j].jsonBlocksList[i].BlockSize + "</label>");
            } else {
                boardAsHTML = boardAsHTML.concat("<label id=\"ColSliceRow" + i + "Column" + j + "\" class=\"slice\"></label>");
            }
        }

        boardAsHTML = boardAsHTML.concat("<br>");
    }

    return boardAsHTML;
}