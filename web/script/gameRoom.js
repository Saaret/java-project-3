/**
 * Created by Saar on 20/10/2016.
 */

var xhrInitRoom = new XMLHttpRequest;
var xhrGetInitPlayers = new XMLHttpRequest;
var xhrDidGameStart = new XMLHttpRequest;
var xhrGetSpectators = new XMLHttpRequest;
var xhrPlayersScores = new XMLHttpRequest;
var xhrGameBoardSpecs = new XMLHttpRequest;
var xhrLeaveGame = new XMLHttpRequest;
var xhrLogOut = new XMLHttpRequest;
var xhrGetCurrentPlayer = new XMLHttpRequest;
var xhrMoveSubmit = new XMLHttpRequest;
var xhrRobotTurn = new XMLHttpRequest;
var xhrEndTurn = new XMLHttpRequest;
var xhrGetBoard = new XMLHttpRequest;
var xhrGetSlices = new XMLHttpRequest;
var xhrDidGameFinish = new XMLHttpRequest;
var xhrGetCurrentPlayerBoard = new XMLHttpRequest;
var xhrUndoMove = new XMLHttpRequest;
var xhrBeginReplay = new XMLHttpRequest;
var xhrEndReplay = new XMLHttpRequest;
var xhrPerformUndo = new XMLHttpRequest;
var xhrPerformRedo = new XMLHttpRequest;
var refreshRate = 2000; //milliseconds
var intervalIdForPlayersInit;
var intervalDidGameStart;
var intervalGetCurrentPlayer;
var intervalDidGameFinish;
var intervalPlayersScores;
var userName;
var isPlayer;
var isInReplay = false;
var didNotifyPlayerAboutTurn = false;

function fetchGameTitle() {
    var query = window.location.search.substring(1);
    $("#roomNameHeader").text(decodeURI(query).split('=')[1]);
}

function getInitRoom() {
    // open the connection
    xhrInitRoom.open('POST', 'GameRoomQueries', false);
    xhrInitRoom.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=InitRoom&GameTitle=" + $("#roomNameHeader").text();
    xhrInitRoom.send(params);
}

xhrInitRoom.onload = function(){
    if (xhrInitRoom.status === 200) {
        console.log("Successfully received game details for init");
        var gameDetails = JSON.parse(xhrInitRoom.responseText);
        initRoom(gameDetails);
    } else {
        console.log("Didn't receive game details for init");
    }
}

function initRoom(gameDetails) {
    $("#maximumTurnsLabel").empty();
    $("#maximumTurnsLabel").text(gameDetails.MaxTurns);
    $("#gameBoardDiv").empty();
    userName = decodeURI(gameDetails.WhoAreYou);
    isPlayer = gameDetails.isPlayer;
    var numOfRows = gameDetails.RowNumber;
    var numOfCols = gameDetails.ColNumber;
    var rowSlices = gameDetails.RowSlices;
    var colSlices = gameDetails.ColSlices;
    var boardAsHTML = generateBoard(numOfRows, numOfCols, rowSlices, colSlices);
    $("#gameBoardDiv").html(boardAsHTML);
}

function checkGameFinished() {
    // open the connection
    xhrDidGameFinish.open('POST', 'GameRoomQueries', false);
    xhrDidGameFinish.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=IsGameWon&GameTitle=" + $("#roomNameHeader").text();
    xhrDidGameFinish.send(params);
}

xhrDidGameFinish.onload = function(){
    if (xhrDidGameFinish.status === 200) {
        console.log("Successfully received game finished details");
        var gameFinishedDetails = JSON.parse(xhrDidGameFinish.responseText);
        if(gameFinishedDetails.success == true) {
            gameFinished(gameFinishedDetails.message);
        }
    } else {
        console.log("Didn't receive game finished details");
    }
}

function getInitPlayers() {
    // open the connection
    xhrGetInitPlayers.open('POST', 'GameRoomQueries', true);
    xhrGetInitPlayers.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=GetInitPlayers&GameTitle=" + $("#roomNameHeader").text();
    xhrGetInitPlayers.send(params);
}

xhrGetInitPlayers.onload = function(){
    if (xhrGetInitPlayers.status === 200) {
        console.log("Successfully received list of players");
        var listOfPlayers = JSON.parse(xhrGetInitPlayers.responseText);
        refreshPlayersTable(listOfPlayers)
    } else {
        console.log("Didn't receive list of players");
    }
}

//players = a list of usernames
function refreshPlayersTable(listOfPlayers) {
    //clear all current users
    $('#playersTableBody').empty();

    if(typeof listOfPlayers != 'undefined') {
        var numOfPlayers = listOfPlayers.playerList.length;
        if (numOfPlayers > 0) {
            var listOfPlayersFromServer = listOfPlayers.playerList;
            $.each(listOfPlayersFromServer, function (index, player) {
                //create a new <option> tag with a value in it and
                //appeand it to the #userslist (div with id=userslist) element
                var playerType = (player.IsComputer == false ? "HUMAN" : "ROBOT");
                var playerScore = player.Score == -1 ? 0 : player.Score;
                $(
                   "<tr>"
                +       "<td>" + decodeURI(player.PlayerName) + "</td>"
                +       "<td id=td" + player.PlayerName + ">" + playerScore + "</td>"
                +       "<td>" + playerType + "</td>"
                +  "</tr>"
                ).appendTo($("#playersTableBody"));
            });
        }
    }
}

function didGameStart() {
    // open the connection
    xhrDidGameStart.open('POST', 'GameRoomQueries', false);
    xhrDidGameStart.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=DidGameStart&GameTitle=" + $("#roomNameHeader").text();
    xhrDidGameStart.send(params);
}

xhrDidGameStart.onload = function(){
    if (xhrDidGameStart.status === 200) {
        console.log("Successfully received did game start response");
        var gameStatusObject = JSON.parse(xhrDidGameStart.responseText);
        handleDidGameStart(gameStatusObject);
    } else {
        console.log("Didn't receieve did game start response");
    }
}

function handleDidGameStart(gameStatusObject) {
    $("#gameRoomStatusLabel").empty();

    if(gameStatusObject.isGameInProgress == false) {
        $("#gameRoomStatusLabel").text("Game is not running");
        $("#currentPlayerLabel").text("*** Game is not running ***");
    } else if (gameStatusObject.isGameInProgress == true) {
        $("#gameRoomStatusLabel").text("GAME IS LIVE!");
        if(isPlayer == true) {
            $("#endGameButton").prop('disabled', true);
        }
        getInitPlayers();
        clearInterval(intervalDidGameStart);
        intervalGetCurrentPlayer = setInterval(callGetCurrentPlayer, refreshRate);
        setInterval(getSpectators, refreshRate);
        intervalDidGameFinish = setInterval(checkGameFinished, refreshRate);
        intervalPlayersScores = setInterval(playersScores, refreshRate);
    } else {
        console.log("There is a problem with gameStatusObject");
    }
}

function getSpectators() {
    // open the connection
    xhrGetSpectators.open('POST', 'GameRoomQueries', true);
    xhrGetSpectators.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=GetSpectators&GameTitle=" + $("#roomNameHeader").text();
    xhrGetSpectators.send(params);
}

xhrGetSpectators.onload = function(){
    if (xhrGetSpectators.status === 200) {
        console.log("Successfully received spectators");
        var spectators = JSON.parse(xhrGetSpectators.responseText);
        setSpectators(spectators);
    } else {
        console.log("Didn't receive spectators");
    }
}

function setSpectators(spectators) {
    if(spectators.spectatorsList != null) {
        $("#spectatorsList").empty();
        $.each(spectators.spectatorsList, function (index, spectator) {
            $("<li>" + decodeURI(spectator) + "</li>").appendTo($("#spectatorsList"));
        });
    } else {
        $("#spectatorsList").empty();
    }
}

function playersScores() {
    // open the connection
    xhrPlayersScores.open('POST', 'GameRoomQueries', true);
    xhrPlayersScores.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=PlayerScores&GameTitle=" + $("#roomNameHeader").text();
    xhrPlayersScores.send(params);
}

xhrPlayersScores.onload = function(){
    if (xhrPlayersScores.status === 200) {
        console.log("Successfully received scores");
        var scores = JSON.parse(xhrPlayersScores.responseText);
        setScores(scores);
    } else {
        console.log("Didn't receive scores");
    }
}

function setScores(scores) {
    $.each(scores, function(name, score) {
        var tdId = "#td" + decodeURI(name);
        $(tdId).text(score);
    });
}

function getGameBoardSpecs() {
    // open the connection
    xhrGameBoardSpecs.open('POST', 'GameBoardSpecs', true);
    xhrGameBoardSpecs.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "GameTitle=" + $("#roomNameHeader").text();
    xhrGameBoardSpecs.send(params);
}

function leaveGame() {
    // open the connection
    xhrLeaveGame.open('POST', 'GameRoomQueries', true);
    xhrLeaveGame.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=LeaveGame&GameTitle=" + $("#roomNameHeader").text();
    xhrLeaveGame.send(params);
    window.location.href = '../Griddler_Game/lobby.html';
}

xhrLeaveGame.onload = function(){
    if (xhrLeaveGame.status === 200) {
        console.log("Player has successfully left the game");
        getInitPlayers();
        callGetCurrentPlayer();
    } else {
        console.log("Player didn't leave the game successfully");
    }
}

function logOut() {
    // open the connection
    xhrLogOut.open('POST', 'LogOut', true);
    xhrLogOut.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    xhrLogOut.send();
    window.location.href = '../Griddler_Game/index.html';
}

xhrLogOut.onload = function(){
    if (xhrLogOut.status === 200) {
        console.log("Player has successfully logged out");
        getInitPlayers();
    } else {
        console.log("The player didn't log out successfully");
    }
}

$(window).on('load', function(){
    $("#moveSubmitForm").on('submit', submitMove);
    fetchGameTitle();
    getInitRoom();
    intervalIdForPlayersInit = setInterval(getInitPlayers, refreshRate);
    if(isPlayer) {
        intervalDidGameStart = setInterval(didGameStart, refreshRate);
    } else {
        setInterval(getSpectators, refreshRate);
        intervalGetCurrentPlayer = setInterval(callGetCurrentPlayer, refreshRate);
        intervalDidGameFinish = setInterval(checkGameFinished, refreshRate);
        intervalPlayersScores = setInterval(playersScores, refreshRate);
        setInterval(callGetCurrentPlayerBoard, refreshRate);
        setInterval(getSlices, refreshRate);
        $("#gameRoomStatusLabel").text("GAME IS LIVE!");
    }
});

var chosenButtons = [];

function cellClicked(button) {
    var rowAndCol = button.id.match(/\d+/g).map(Number);
    var row = rowAndCol[0];
    var col = rowAndCol[1];
    if($.inArray("selectedCell", button.classList) == -1) {
        // the button was not selected before
        var oldType = "undefinedCell";
        if($.inArray("whiteCell", button.classList) > -1) {
            // the button is white
            oldType = "white";
            $(button).removeClass('whiteCell');
        } else if($.inArray("blackCell", button.classList) > -1) {
            // the button is black
            oldType = "black";
            $(button).removeClass('blackCell');
        }
        var objToAdd = JSON.parse('{ "x":"'+col+'", "y":"'+row+'", "oldType":"'+oldType+'" }');
        chosenButtons.push(objToAdd);
        $(button).addClass('selectedCell');
    } else {
        // the button was selected before
        $.each(chosenButtons, function(index, currentButton){
            if((currentButton.x == col)&&(currentButton.y == row)) {
                $(button).removeClass('selectedCell');
                if(currentButton.oldType == "white") {
                    $(button).addClass('whiteCell')
                } else if (currentButton.oldType == "black") {
                    $(button).addClass('blackCell');
                }
                chosenButtons.splice(index, 1);
            }
        });
    }

    $("#undoMoveButton").prop('disabled', (chosenButtons.length > 0));
    $("#endTurnButton").prop('disabled', (chosenButtons.length > 0));
    $("#startReplayButton").prop('disabled', (chosenButtons.length > 0) || (numOfMovesDone == 0));
}

function callGetCurrentPlayer() {
    // open the connection
    xhrGetCurrentPlayer.open('POST', 'GameRoomQueries', true);
    xhrGetCurrentPlayer.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=GetCurrentPlayer&GameTitle=" + $("#roomNameHeader").text();
    xhrGetCurrentPlayer.send(params);
}

xhrGetCurrentPlayer.onload = function(){
    if (xhrGetCurrentPlayer.status === 200) {
        console.log("Successfully received current player");
        var currentPlayer = JSON.parse(xhrGetCurrentPlayer.responseText);
        getCurrentPlayer(currentPlayer);
    } else {
        console.log("Didn't receive current player");
    }
}

function getCurrentPlayer(currentPlayer) {
    $("#currentPlayerLabel").text(decodeURI(currentPlayer.PlayerName));
    if(currentPlayer.Score > -1) {
        var tdId = "#td" + decodeURI(currentPlayer.PlayerName);
        $(tdId).text(currentPlayer.Score);
    }
    if(isPlayer == false) {
        $("#currentTurnLabel").text(parseInt(currentPlayer.CurrentTurn));
    }
    if(decodeURI(currentPlayer.PlayerName) == userName) {
        clearInterval(intervalGetCurrentPlayer);
        if(currentPlayer.IsComputer == false) {
            if(isInReplay == true) {
                if(didNotifyPlayerAboutTurn == false) {
                    alert("It's your turn now!");
                    didNotifyPlayerAboutTurn = true;
                }
            } else {
                startTurn();
            }
        } else {
            $("#endGameButton").prop('disabled', true);
            doRobotTurn();
            doRobotTurn();
            callEndTurn();
        }
        checkGameFinished();
    }
}

function startTurn() {
    enableGameCells();
    $("#moveSubmitButton").prop('disabled', false);
    $("#endTurnButton").prop('disabled', false);
    $("#endGameButton").prop('disabled', false);
    if(numOfMovesDone > 0) {
        $("#undoMoveButton").prop('disabled', false);
        $("#startReplayButton").prop('disabled', false);
    } else {
        $("#undoMoveButton").prop('disabled', true);
        $("#startReplayButton").prop('disabled', true);
    }
}

function enableGameCells() {
    var allGameCells = $("#gameBoardDiv > label.btn");
    $.each(allGameCells, function(index, gameCell){
        $(gameCell).removeAttr('disabled');
        $(gameCell).attr('onclick', 'cellClicked(this)');
    });
}

function callEndTurn() {
    // open the connection
    xhrEndTurn.open('POST', 'GameRoomQueries', true);
    xhrEndTurn.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=EndTurn&GameTitle=" + $("#roomNameHeader").text();
    xhrEndTurn.send(params);
}

xhrEndTurn.onload = function(){
    if (xhrEndTurn.status === 200) {
        console.log("Successfully receive end turn");
        //var endTurnObj = JSON.parse(xhrGetCurrentPlayer.responseText);
        //endTurn(endTurnObj);
        endTurn();
    } else {
        console.log("Didn't receive end turn");
    }
}

function endTurn() {
    lastMoveEnded();
    $("#currentMoveLabel").text(0);
    $("#currentTurnLabel").text(parseInt($("#currentTurnLabel").text()) + 1);
    $("#endTurnButton").prop('disabled', true);
    $("#endGameButton").prop('disabled', true);
    $("#undoMoveButton").prop('disabled', true);
    intervalGetCurrentPlayer = intervalGetCurrentPlayer = setInterval(callGetCurrentPlayer, refreshRate);
}

function lastMoveEnded() {
    disableGameCells();
    $("#moveSubmitButton").prop('disabled', true);
}

function disableGameCells() {
    var allGameCells = $("#gameBoardDiv > label.btn");
    $.each(allGameCells, function(index, gameCell){
        $(gameCell).attr('disabled', 'disabled');
        $(gameCell).removeAttr('onclick');
    });
}

var gameMove;
var numOfMovesDone = 0;

function submitMove(event) {
    event.stopPropagation();
    event.preventDefault();
    var moveColor = $('input[name="color"]:checked').val().toUpperCase();
    console.log(moveColor);
    $.each(chosenButtons, function(index, currentButton) {
        delete currentButton.oldType;
    });
    gameMove = JSON.parse("{ \"moveColor\":\"" + moveColor + "\", \"chosenCells\":\"\" }");
    gameMove.chosenCells = chosenButtons;
    console.log(gameMove);
    sendMoveToServer();
}

function sendMoveToServer() {
    // open the connection
    xhrMoveSubmit.open('POST', 'GameRoomQueries', true);
    xhrMoveSubmit.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=MoveSubmit&GameTitle=" + $("#roomNameHeader").text() + "&GameMove="+JSON.stringify(gameMove);
    xhrMoveSubmit.send(params);
}

xhrMoveSubmit.onload = function(){
    if (xhrMoveSubmit.status === 200) {
        console.log("Successfully submitted move");
        var gameDetails = JSON.parse(xhrMoveSubmit.responseText);
        moveHasBeenSubmitted();
    } else {
        console.log("Didn't submit move");
        alert("Error! Move was not submitted!");
    }
}

function moveHasBeenSubmitted() {
    var submittedColor = gameMove.moveColor;
    var submittedCells = gameMove.chosenCells;
    var gameCell;
    $.each(submittedCells, function(index, currentCell){
        gameCellId = "GameCellRow" + currentCell.y + "Column" + currentCell.x;
        gameCell = $("#" + gameCellId)[0];
        $(gameCell).removeClass('selectedCell');
        if(submittedColor == "WHITE") {
            $(gameCell).addClass('whiteCell');
        } else if (submittedColor == "BLACK") {
            $(gameCell).addClass('blackCell');
        }
    });
    chosenButtons = [];
    $("#endTurnButton").prop('disabled', false);
    numOfMovesDone++;
    handleUndoButton();
    $("#startReplayButton").prop('disabled', false);
    handleMoveLabel();
    getSlices();
}

function handleUndoButton() {
    $("#undoMoveButton").prop('disabled', (numOfMovesDone == 0));
}

function handleMoveLabel() {
    var nextMoveNumber = parseInt($("#currentMoveLabel").text()) + 1;
    $("#currentMoveLabel").text(nextMoveNumber);
    if(nextMoveNumber == 2) {
        lastMoveEnded();
    }
}

function doRobotTurn() {
    // open the connection
    xhrRobotTurn.open('POST', 'GameRoomQueries', false);
    xhrRobotTurn.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=AITurn&GameTitle=" + $("#roomNameHeader").text();
    xhrRobotTurn.send(params);
}

xhrRobotTurn.onload = function(){
    if (xhrRobotTurn.status === 200) {
        console.log("Successfully did robot turn");
        robotTurnIsOver();
    } else {
        console.log("Didn't do robot turn");
    }
}

function getBoard() {
    // open the connection
    xhrGetBoard.open('POST', 'GameRoomQueries', false);
    xhrGetBoard.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=getBoard&GameTitle=" + $("#roomNameHeader").text();
    xhrGetBoard.send(params);
}

function getSlices() {
    // open the connection
    xhrGetSlices.open('POST', 'GameRoomQueries', false);
    xhrGetSlices.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=getSlices&GameTitle=" + $("#roomNameHeader").text();
    xhrGetSlices.send(params);
}

xhrGetBoard.onload = function(){
    if (xhrGetBoard.status === 200) {
        console.log("Successfully received board");
        var board = JSON.parse(xhrGetBoard.responseText);
        setBoardFromServer(board);
    } else {
        console.log("Didn't receive board");
    }
}

xhrGetSlices.onload = function(){
    if (xhrGetSlices.status === 200) {
        console.log("Successfully received slices");
        var slices = JSON.parse(xhrGetSlices.responseText);
        setSlicesFromServer(slices);
    } else {
        console.log("Didn't receive slices");
    }
}

function robotTurnIsOver() {
    getBoard();
    // TODO: update slices
    $("#currentMoveLabel").text(parseInt($("#currentMoveLabel").text()) + 1);
    intervalGetCurrentPlayer = setInterval(callGetCurrentPlayer, refreshRate);
}

function setBoardFromServer(board) {
    $.each(board.GameBoard, function(index1, currentRow) {
        $.each(currentRow, function(index2, currentCell) {
            var currentCellId = "GameCellRow" + currentCell.Y + "Column" + currentCell.X;
            if ($("#" + currentCellId).hasClass("whiteCell") == true) {
                $("#" + currentCellId).removeClass("whiteCell");
            }
            if ($("#" + currentCellId).hasClass("blackCell") == true) {
                $("#" + currentCellId).removeClass("blackCell");
            }
            if (currentCell.CellStatus == "WHITE") {
                $("#" + currentCellId).addClass("whiteCell");
            }
            if (currentCell.CellStatus == "BLACK") {
                $("#" + currentCellId).addClass("blackCell");
            }
        });
    });
}

function setSlicesFromServer(slices){
    var rowSlices = slices.RowSlices.SliceList;
    var blockId;
    var isPerfect;
    var i;
    for(i = 0; i < rowSlices.length; i ++) {
        for(j = 0; j < rowSlices[i].jsonBlocksList.length; j++) {
            blockId = "RowSliceRow" + i + "Column" + j;
            isPerfect = rowSlices[i].jsonBlocksList[j].IsPerfect;
            setPerfect(blockId, isPerfect);
        }
    }

    var colSlices = slices.ColSlices.SliceList;
    for(i = 0; i < colSlices.length; i++) {
        for(j = 0; j < colSlices[i].jsonBlocksList.length; j++) {
            blockId = "ColSliceRow" + j + "Column" + i;
            isPerfect = colSlices[i].jsonBlocksList[j].IsPerfect;
            setPerfect(blockId, isPerfect);
        }
    }
}

function setPerfect(blockId, isPerfect) {
    if(($("#" + blockId).hasClass("perfectSlice") == true) && (isPerfect == false)) {
        $("#" + blockId).removeClass("perfectSlice");
    } else if (($("#" + blockId).hasClass("perfectSlice") == false) && (isPerfect == true)) {
        $("#" + blockId).addClass("perfectSlice");
    }
}

function gameFinished(message) {
    alert(message);
    clearInterval(intervalGetCurrentPlayer);
    disableGameCells()
    $("#endTurnButton").prop('disabled', true);
    $("#undoMoveButton").prop('disabled', true);
    $("#moveSubmitButton").prop('disabled', true);
    $("#endGameButton").prop('disabled', false);
    clearInterval(intervalDidGameFinish);
    clearInterval(intervalPlayersScores);
}

function callUndoMove() {
    // open the connection
    xhrUndoMove.open('POST', 'GameRoomQueries', true);
    xhrUndoMove.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=PerformUndo&GameTitle=" + $("#roomNameHeader").text();
    xhrUndoMove.send(params);
}

xhrUndoMove.onload = function(){
    if (xhrUndoMove.status === 200) {
        console.log("Successfully received undo move");
        var undoneMoveObj = JSON.parse(xhrUndoMove.responseText);
        undoMove(undoneMoveObj);
    } else {
        console.log("Didn't receive undo move");
    }
}

function undoMove(undoneMoveObj) {
    if(undoneMoveObj.success == true) {
        numOfMovesDone--;
        startTurn();
        getBoard();
        getSlices();
        var moveNum = parseInt($("#currentMoveLabel").text());
        if(moveNum > 0) {
            $("#currentMoveLabel").text(moveNum - 1);
        }
        if(numOfMovesDone == 0) {
            $("#startReplayButton").prop('disabled', true);
        }
    } else {
        alert("Couldn't undo last move!");
    }
}

function callGetCurrentPlayerBoard() {
    // open the connection
    xhrGetCurrentPlayerBoard.open('POST', 'GameRoomQueries', true);
    xhrGetCurrentPlayerBoard.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=GetCurrentPlayerBoard&GameTitle=" + $("#roomNameHeader").text();
    xhrGetCurrentPlayerBoard.send(params);
}

xhrGetCurrentPlayerBoard.onload = function(){
    if (xhrGetCurrentPlayerBoard.status === 200) {
        console.log("Successfully received current player's board");
        var board = JSON.parse(xhrGetCurrentPlayerBoard.responseText);
        setBoardFromServer(board);
    } else {
        console.log("Didn't receive current player's board");
    }
}

function startReplayMode() {
    isInReplay = true;
    // open the connection
    xhrBeginReplay.open('POST', 'GameRoomQueries', true);
    xhrBeginReplay.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=BeginReplay&GameTitle=" + $("#roomNameHeader").text();
    xhrBeginReplay.send(params);
}

xhrBeginReplay.onload = function(){
    if (xhrBeginReplay.status === 200) {
        var replayModeObj = JSON.parse(xhrBeginReplay.responseText);
        console.log("Successfully started replay");
        $("#startReplayButton").prop('disabled', true);
        $("#prevButton").prop('disabled', replayModeObj.remainingUndo == 0) ;
        boardDisabledInReplay = $("#GameCellRow0Column0").is(':disabled');
        submitMoveInReplay = $("#moveSubmitButton").is(':disabled');
        undoMoveInReplay = $("#undoMoveButton").is(':disabled');
        endTurnInReplay = $("#endTurnButton").is(':disabled');
        leaveGameInReplay = $("#endGameButton").is(':disabled');
        disableGameCells();
        $("#moveSubmitButton").prop('disabled', true);
        $("#endTurnButton").prop('disabled', true);
        $("#endGameButton").prop('disabled', true);
        $("#undoMoveButton").prop('disabled', true);
        $("#exitReplayButton").prop('disabled', false);
    } else {
        console.log("Didn't start replay");
    }
}

function callPrev() {
    // open the connection
    xhrPerformUndo.open('POST', 'GameRoomQueries', true);
    xhrPerformUndo.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=PerformUndo&GameTitle=" + $("#roomNameHeader").text();
    xhrPerformUndo.send(params);
}

xhrPerformUndo.onload = function(){
    if (xhrPerformUndo.status === 200) {
        console.log("Successfully received prev move");
        var prevMoveObj = JSON.parse(xhrPerformUndo.responseText);
        prevMove(prevMoveObj);
    } else {
        console.log("Didn't receive prev move");
    }
}

function prevMove(prevMoveObj) {
    if(prevMoveObj.success == true) {
        getBoard();
        getSlices();
        $("#nextButton").prop('disabled', false);
        if(prevMoveObj.message == 0) {
            $("#prevButton").prop('disabled', true);
        }
    } else {
        alert("Couldn't get previous move!");
    }
}

function callNext() {
    // open the connection
    xhrPerformRedo.open('POST', 'GameRoomQueries', true);
    xhrPerformRedo.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=PerformRedo&GameTitle=" + $("#roomNameHeader").text();
    xhrPerformRedo.send(params);
}

xhrPerformRedo.onload = function(){
    if (xhrPerformRedo.status === 200) {
        console.log("Successfully received next move");
        var nextMoveObj = JSON.parse(xhrPerformRedo.responseText);
        nextMove(nextMoveObj);
    } else {
        console.log("Didn't receive next move");
    }
}

function nextMove(nextMoveObj) {
    if(nextMoveObj.success == true) {
        $("#prevButton").prop('disabled', false);
        getBoard();
        getSlices();
        if(nextMoveObj.message == 0) {
            $("#nextButton").prop('disabled', true);
        }
    } else {
        alert("Couldn't get previous move!");
    }
}

function callExitReplayMode() {
    // open the connection
    xhrEndReplay.open('POST', 'GameRoomQueries', true);
    xhrEndReplay.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the Data
    var params = "RequestType=EndReplay&GameTitle=" + $("#roomNameHeader").text();
    xhrEndReplay.send(params);
}

xhrEndReplay.onload = function(){
    if (xhrEndReplay.status === 200) {
        console.log("Successfully ended replay");
        var endReplayObj = JSON.parse(xhrEndReplay.responseText);
        endReplay(endReplayObj);
    } else {
        console.log("Didn't end replay");
    }
}

function endReplay(endReplayObj) {
    if(endReplayObj.success == true) {
        isInReplay = false;
        $("#startReplayButton").prop('disabled', false);
        $("#prevButton").prop('disabled', true);
        $("#nextButton").prop('disabled', true);
        $("#exitReplayButton").prop('disabled', true);
        getBoard();
        getSlices();
        if(didNotifyPlayerAboutTurn == true) {
            didNotifyPlayerAboutTurn = false;
            startTurn();
        } else {
            (boardDisabledInReplay == true) ? disableGameCells() : enableGameCells();
            $("#moveSubmitButton").prop('disabled', submitMoveInReplay);
            $("#undoMoveButton").prop('disabled', undoMoveInReplay);
            $("#endTurnButton").prop('disabled', endTurnInReplay);
            $("#endGameButton").prop('disabled', leaveGameInReplay);
        }
    } else {
        alert(endReplayObj.message);
    }
}

var boardDisabledInReplay;
var submitMoveInReplay;
var undoMoveInReplay;
var endTurnInReplay;
var leaveGameInReplay;